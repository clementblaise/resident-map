# resident-map

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development

The development version use api mocking 

```
yarn serve
```

### Compiles and hot-reloads for production

The production version use the real api server 

```
yarn run servepord
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
