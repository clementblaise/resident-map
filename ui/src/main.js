import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from '@/store/index'
import axios from 'axios'
import vuetify from './plugins/vuetify';


Vue.config.productionTip = false

axios
    .get('/config.json')
    .then(({data}) => {
        Vue.prototype.$mapboxToken = data.mapboxtoken
        Vue.prototype.$mapboxStyle = data.mapboxstyle
        Vue.prototype.$spotifyClientID = data.spotify
        Vue.prototype.$lastFMAPIKey = data.latfm
        Vue.prototype.$spotifyRedirectCallback = data.spotifyRedirect
        store.dispatch('loadEndpointSpotify', data.thirdParty)

        new Vue({
            router,
            render: h => h(App),
            vuetify,
            store
        }).$mount('#app')
    })



