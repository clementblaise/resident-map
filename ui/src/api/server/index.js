import axios from 'axios'
import gql from 'graphql-tag';
import ApolloClient from "apollo-boost";
import moment from "moment";
import {uniqBy} from "../../utils";

const apollo = new ApolloClient({
    uri: 'https://api.residentmap.ml'
});


function requestEvents(zone, lastfmID, spotifyID, date) {
    return new Promise((resolve, reject) => {
        apollo.query({
            query: gql`
                {
                    events(area: "${zone}", lastfmID: "${lastfmID}", spotifyID: "${spotifyID}", date: "${date}") {
                        name
                        participant
                        address
                        lat
                        lon
                        dateStart
                        dateEnd
                        lasfmArtist
                        spotifyArtist
                        otherArtist
                        venueName
                        dateStart
                        dateEnd
                        flyerPath
                    }
                }
              `,
        })
            .then(response => resolve(
                response.data.events.map(event => {
                    event.showDetails = false
                    return event
                })
            ))
            .catch(err => reject(err))

    })
}

function requestSearch(text) {
    return new Promise((resolve, reject) => {
        apollo.query({
            query: gql`
                {
                  search(text: "${text}" ){
                    name
                    type
                    area
                    date
                  }
                }
              `,
        })
            .then(({data}) => {
                data.search.forEach(result => {
                    if (result.type === "event") result.date = moment(result.date)
                    return result
                })
                resolve(data.search)
            })
            .catch(err => reject(err))

    })
}

function requestSpotifyCount(id) {
    return new Promise((resolve, reject) => {
        apollo.query({
            query: gql`
                {
                  spotify(spotifyID: "${id}" ){
                    count
                  }
                }
              `,
        })
            .then(({data}) => resolve(data.spotify[0].count))
            .catch(err => reject(err))
    })
}

function requestLastFMCount(id) {
    return new Promise((resolve, reject) => {
        apollo.query({
            query: gql`
                {
                  lastfm(lastfmID: "${id}" ){
                    count
                  }
                }
              `,
        })
            .then(({data}) => resolve(data.lastfm[0].count))
            .catch(err => reject(err))
    })
}


function requestLocation(endpoint) {
    return new Promise((resolve, reject) => {
        axios
            .get(endpoint + '/location')
            .then(response => resolve(response.data))
            .catch(err => reject(err))
    })

}

function requestGeoToArea(lat, lon) {
    return new Promise((resolve, reject) => {
        apollo.query({
            query: gql`
                {
                  locate(lat: ${lat}, lon: ${lon} ){
                    fullName
                  }
                }
              `,
        })
            .then(({data}) => resolve(data.locate[0].fullName))
            .catch(err => reject(err))

    })
}

function requestZones() {
    return new Promise((resolve, reject) => {
        apollo.query({
            query: gql`
                {
                    zone {
                        country
                        isoCode
                        area {
                            name
                            fullName
                        }
                    }
                }
              `,
        })
            .then(response => resolve(response.data.zone))
            .catch(err => reject(err))
    })

}

function requestSpotifyPlaylists(endpoint, id) {
    return new Promise((resolve, reject) => {
        if (id === '') reject()
        axios
            .get(endpoint + '/playlists/' + id)
            .then(response => resolve(response.data))
            .catch(err => reject(err))
    })
}

function requestSpotifyMatchedArtists(endpoint, id) {
    return new Promise((resolve, reject) => {
        if (id === '') reject()
        axios
            .get(endpoint + '/matches/' + id)
            .then(response => resolve(response.data))
            .catch(err => reject(err))
    })
}


function requestSpotifyAnalyse(endpoint, id, params) {
    return new Promise((resolve, reject) => {
        if (id === '') reject()
        axios
            .get(endpoint + '/analyse/' + id, {params: params})
            .then(response => resolve(response.data))
            .catch(err => reject(err))
    })
}

function requestLastFMAnalyse(endpoint, id) {
    return new Promise((resolve, reject) => {
        if (id === '') reject()
        axios
            .get(endpoint + '/sync/' + id)
            .then(response => resolve(response.data))
            .catch(err => reject(err))
    })
}

function requestFeatureArtist(area, spotifyID, lastfmID) {
    return new Promise((resolve, reject) => {
        apollo.query({
            query: gql`
                {
                  featureArtist(area: "${area}", spotifyID: "${spotifyID}", lastfmID : "${lastfmID}"){
                    all{
                      name
                      img
                      events
                      rank
                    }
                    lastfm{
                      name
                      img
                      events
                      rank
                    }
                    spotify{
                      name
                      img
                      events
                      rank
                    }
                  }
                }
              `,
        })
            .then(({data}) => {
                let artists = []

                if (data.featureArtist.lastfm !== null) artists.push(data.featureArtist.lastfm.map(artist => {
                    artist.source = 'lastfm'
                    artist.color = "border-color:#e41c24"
                    return artist
                }))

                if (data.featureArtist.spotify !== null) artists.push(data.featureArtist.spotify.map(artist => {
                    artist.source = 'spotify'
                    artist.color = "border-color:#20d760"
                    return artist
                }))

                if (data.featureArtist.all !== null) artists.push(data.featureArtist.all.map(artist => {
                    artist.color = "border-color:transparent"
                    return artist
                }))
                resolve(uniqBy(artists.flat(), 'name'))
            })
            .catch(err => reject(err))
    })
}

function requestArtist(name) {
    return new Promise((resolve, reject) => {
        apollo.query({
            query: gql`
                {
                  eventArtist(name: "${name}"){
                    img
                    events{
                      name
                      participant
                      address
                      lat
                      lon
                      dateStart
                      dateEnd
                      venueName
                      flyerPath
                      area
                    }
                  }
                }
              `,
        })
            .then(({data}) => {
                let info = data.eventArtist
                info.events = info.events.map(event => {
                    event.showDetails = false
                    event.date = moment(event.dateStart).format('YYYY-MM-DD')
                    return event
                })
                // eslint-disable-next-line no-console
                resolve(info)
            })
            .catch(err => reject(err))
    })
}

export default {
    requestEvents,
    requestSearch,
    requestSpotifyCount,
    requestLastFMCount,
    requestLocation,
    requestGeoToArea,
    requestZones,
    requestSpotifyMatchedArtists,
    requestSpotifyPlaylists,
    requestSpotifyAnalyse,
    requestLastFMAnalyse,
    requestFeatureArtist,
    requestArtist
}
