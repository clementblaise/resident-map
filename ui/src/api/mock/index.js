import eventsParis from './data/events_paris'
import eventSouthWest from './data/events_south_west'
import eventFinland from './data/events_finland'

const requestEvents = (zone) => new Promise(resolve => {
    if (zone === 'France - Paris') setTimeout(() => resolve(eventsParis.map(event => {
        event.showDetails = false
        return event
    })), 500)
    else if (zone === 'France - South West') setTimeout(() => resolve(eventSouthWest.map(event => {
        event.showDetails = false
        return event
    })), 500)
    else setTimeout(() => resolve(eventFinland).map(event => {
            event.showDetails = false
            event.hover = false
            return event
        }), 500)

})

import location from './data/location'

const requestLocation = () => new Promise(resolve => {
    setTimeout(() => resolve(location), 500)
})

import zones from './data/zones'

const requestZones = () => new Promise(resolve => {
    setTimeout(() => resolve(zones), 500)
})

const requestSpotifyAnalyse = () => new Promise(resolve => {
    setTimeout(() => resolve({add: 10}), 500)
})

const requestLasftFMAnalyse = () => new Promise(resolve => {
    setTimeout(() => resolve({add: 20}), 500)
})

import artists from './data/artists'

const requestSpotifyMatchedArtists = () => new Promise(resolve => {
    setTimeout(() => resolve(artists), 1000)
})

import playlists from './data/playlists'

const requestSpotifyPlaylists = () => new Promise(resolve => {
    setTimeout(() => resolve(playlists), 1000)
})


export default {
    requestEvents,
    requestLocation,
    requestSpotifyMatchedArtists,
    requestSpotifyPlaylists,
    requestZones,
    requestSpotifyAnalyse,
    requestLasftFMAnalyse
}