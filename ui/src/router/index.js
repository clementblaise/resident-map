import Vue from 'vue'
import VueRouter from 'vue-router'
import Event from '../views/Event.vue'

Vue.use(VueRouter)


const routes = [
    {
        path: '/',
        name: 'events',
        component: Event
    },
    {
        path: '/artists',
        name: 'artists',
        component: () => import('../views/Artist.vue')
    },
    {
        path: '/spotify/manage',
        name: 'spotifyManage',
        component: () => import('../views/Spotify.vue')
    },
    {
        path: '/spotify/callback/:id',
        name: 'spotifyCallback',
        component: () => import('../components/Spotify/SaveUserID.vue'),
        props: true
    },
    {
        path: '/lastfm/callback/:id',
        name: 'lastfmCallback',
        component: () => import('../components/LastFM/SaveUserID.vue'),
        props: true
    },
    {
        path: '*',
        redirect: '/'
    }
]

const router = new VueRouter({
    //mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
