export function uniqBy(arr, key) {
    let seen = new Set()
    return arr.filter(it => {
        let val = it[key]
        if (seen.has(val)) {
            return false
        } else {
            seen.add(val)
            return true
        }
    })
}

export function groupBy(xs, key) {
    return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
}
