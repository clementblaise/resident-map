import Vue from 'vue'
import Vuex from 'vuex'
import api from 'api-client'
import {uniqBy, groupBy} from '../utils'
import moment from "moment";

Vue.use(Vuex)

const state = {
    events: [],
    eventLastSelected: {},
    eventFilter: "",
    location: {},
    zones: [],
    date: {selected: moment(new Date()), prevMonday: moment(new Date()).startOf('isoWeek')},
    artist: {feature: [], selected: "", info: "", eventSelected: {}, eventLastSelected: {}},
    searchResult: [],
    searchText: "",
    selectedZone: "",
    spotify: {
        id: '',
        matchedArtists: [],
        createdPlaylists: [],
        subscribedPlaylists: [],
        analyseResult: "",
        analyseLock: false,
        count: ""
    },
    lastfm: {id: '', analyseResult: "", analyseLock: false, count: ""},
    endpoint: {event: '', spotify: ''}
}

const actions = {
    loadEvents({commit}) {
        return api
            .requestEvents(state.selectedZone, state.lastfm.id, state.spotify.id, state.date.selected.toString())
            .then(events => commit('SET_EVENTS', events))

    },
    searchText({commit}, text) {
        return api
            .requestSearch(text)
            .then(result => {
                commit('SET_SEARCH_RESULT', result)
                return result
            })


    },
    eventShowDetails({commit}, name) {
        commit('SET_EVENT_SHOW_DETAILS', name)
        commit('SET_EVENT_LAST_SELECTED', state.events.filter(event => event.name === name)[0])
    },
    eventHideDetails({commit}) {
        commit('UNSET_EVENT_SHOW_DETAILS')
    },
    eventShowNextDetails({commit}) {
        let next = state.events.findIndex(event => event.name === state.eventLastSelected.name)
        commit('SET_EVENT_SHOW_DETAILS', next)
        commit('SET_EVENT_LAST_SELECTED', state.events[next])
    },
    loadLocation({commit}) {
        return api
            .requestLocation(state.endpoint.spotify)
            .then(location => commit('SET_LOCATION', location))
    },
    geoToArea({commit}) {
        return api
            .requestGeoToArea(Number(state.location['geoplugin_latitude']), Number(state.location['geoplugin_longitude']))
            .then(location => commit('SET_SELECTED_ZONE', location))
    },
    loadZones({commit}) {
        return api
            .requestZones()
            .then(zones => commit('SET_ZONES', zones))
    },
    changeZone({commit}, zone) {
        commit('SET_SELECTED_ZONE', zone)
    },
    saveSpotifyID({commit}, id) {
        commit('SET_SPOTIFY_ID', id)
    },
    saveLastFMID({commit}, id) {
        commit('SET_LASTFM_ID', id)
    },
    analyseSpotify({commit}, params) {
        if (state.spotify.analyseLock) return null
        commit('SET_SPOTIFY_ANALYSE_LOCK', true)
        return api
            .requestSpotifyAnalyse(state.endpoint.spotify, state.spotify.id, params)
            .then(result => {
                commit('SET_SPOTIFY_ANALYSE_RESULT', result)
                commit('SET_SPOTIFY_ANALYSE_LOCK', false)
                return result
            })

    },
    analyseLastFM({commit}) {
        if (state.spotify.analyseLock) return null
        commit('SET_LASTFM_ANALYSE_LOCK', true)
        return api
            .requestLastFMAnalyse(state.endpoint.spotify, state.lastfm.id)
            .then(result => {
                commit('SET_LASTFM_ANALYSE_RESULT', result)
                commit('SET_LASTFM_ANALYSE_LOCK', false)
            })

    },
    loadSpotifyPlaylist({commit}) {
        return api
            .requestSpotifyPlaylists(state.endpoint.spotify, state.spotify.id)
            .then(playlist => {
                commit('SET_SPOTIFY_CREATED_PLAYLISTS', playlist.created)
                commit('SET_SPOTIFY_SUBSCRIBED_PLAYLISTS', playlist.subscribed)
            })
    },
    loadSpotifyMatchedArtists({commit}) {
        return api
            .requestSpotifyMatchedArtists(state.endpoint.spotify, state.spotify.id)
            .then(artist => commit('SET_SPOTIFY_MATCH_ARTIST', artist))
    },
    loadEndpointSpotify({commit}, endpoint) {
        commit('SET_ENDPOINT_SPOTIFY', endpoint)
    },
    loadEndpointEvent({commit}, endpoint) {
        commit('SET_ENDPOINT_EVENT', endpoint)
    },
    modifyDate({commit}, date) {
        commit('SET_DATE', date)
    },
    modifyLastMonday({commit}, date) {
        commit('SET_LAST_MONDAY', date)
    },
    loadSpotifyCount({commit}) {
        return api
            .requestSpotifyCount(state.spotify.id)
            .then(count => commit('SET_SPOTIFY_ARTIST_COUNT', count))
    },
    loadLastFMCount({commit}) {
        return api
            .requestLastFMCount(state.lastfm.id)
            .then(count => commit('SET_LASTFM_ARTIST_COUNT', count))
    },
    loadFeatureArtist({commit}) {
        return api
            .requestFeatureArtist(state.selectedZone, state.spotify.id, state.lastfm.id)
            .then(artists => commit('SET_ARTIST_FEATURE', artists))
    },
    loadArtistInfo({commit}) {
        if (state.artist.selected === '') return
        return api
            .requestArtist(state.artist.selected)
            .then(info => commit('SET_ARTIST_INFO', info))
    },
    modifyArtist({commit}, artist) {
        commit('SET_ARTIST_SELECTED', artist)
    },
    modifyArtistEvent({commit}, name) {
        commit('SET_EVENT_ARTIST', name)
        commit('SET_EVENT_ARTIST_LAST', state.artist.info.events.filter(event => event.name === name)[0])
    },
    hideArtistDetails({commit}) {
        commit('UNSET_EVENT_ARTIST')
    }

}

const mutations = {
    SET_EVENTS(state, events) {
        state.events = events
    },
    SET_SEARCH_TEXT(state, text) {
        state.searchText = text
    },
    SET_SEARCH_RESULT(state, result) {
        state.searchResult = result
    },
    SET_EVENT_SHOW_DETAILS(state, name) {
        state.events.forEach(event => event.showDetails = (event.name === name) ? !event.showDetails : false)
    },
    UNSET_EVENT_SHOW_DETAILS(state) {
        state.events.forEach(event => event.showDetails = false)
    },
    SET_EVENT_LAST_SELECTED(state, event) {
        state.eventLastSelected = event
    },
    SET_LOCATION(state, location) {
        state.location = location
    },
    SET_DATE(state, date) {
        state.date.selected = moment(date)
    },
    SET_LAST_MONDAY(state, date) {
        state.date.prevMonday = moment(date)
    },
    SET_ZONES(state, zones) {
        state.zones = zones
    },
    SET_SELECTED_ZONE(state, zone) {
        state.selectedZone = zone
    },
    SET_SPOTIFY_ID(state, id) {
        state.spotify.id = id
    },
    SET_SPOTIFY_ANALYSE_LOCK(state, value) {
        state.spotify.analyseLock = value
    },
    SET_SPOTIFY_ANALYSE_RESULT(state, result) {
        state.spotify.analyseResult = result
    },
    SET_LASTFM_ID(state, id) {
        state.lastfm.id = id
    },
    SET_LASTFM_ANALYSE_LOCK(state, value) {
        state.lastfm.analyseLock = value
    },
    SET_LASTFM_ANALYSE_RESULT(state, result) {
        state.lastfm.analyseResult = result
    },
    SET_SPOTIFY_CREATED_PLAYLISTS(state, playlists) {
        state.spotify.createdPlaylists = playlists
    },
    SET_SPOTIFY_SUBSCRIBED_PLAYLISTS(state, playlists) {
        state.spotify.subscribedPlaylists = playlists
    },
    SET_SPOTIFY_MATCH_ARTIST(state, artists) {
        state.spotify.matchedArtists = artists
    },
    SET_ENDPOINT_SPOTIFY(state, endpoint) {
        state.endpoint.spotify = endpoint
    },
    SET_ENDPOINT_EVENT(state, endpoint) {
        state.endpoint.event = endpoint
    },
    SET_SPOTIFY_ARTIST_COUNT(state, count) {
        state.spotify.count = count
    },
    SET_LASTFM_ARTIST_COUNT(state, count) {
        state.lastfm.count = count
    },
    SET_ARTIST_FEATURE(state, artists) {
        state.artist.feature = artists
    },
    SET_ARTIST_SELECTED(state, name) {
        state.artist.selected = name
    },
    SET_ARTIST_INFO(state, info) {
        state.artist.info = info
    },
    SET_EVENT_ARTIST(state, name) {
        state.artist.info.events.forEach(event => event.showDetails = (event.name === name) ? !event.showDetails : false)
    },
    SET_EVENT_ARTIST_LAST(state, event) {
        state.artist.eventLastSelected = event
    },
    UNSET_EVENT_ARTIST(state) {
        state.artist.info.events.forEach(event => event.showDetails = false)
    },
}


const getters = {
    getEvents: state => uniqBy(state.events, "name"),
    getEventsOnDate: state => uniqBy(state.events, "name")
        .filter(event => {
            return moment(event.dateStart).isSame(state.date.selected, 'day')
        }),

    getEventsFilterCoordinates: state => uniqBy(state.events, "name")
        .filter(event => event.lat !== null || event.lon !== null)
        .filter(event => moment(event.dateStart).isSame(moment(state.date.selected), 'day')),

    getSearchText: state => state.searchText,
    getSearchResult: state => state.searchResult,
    getEventLastSelected: state => state.eventLastSelected,
    getFilteredEvent: state =>
        state.eventFilter !== '' ?
            state.events.filter(event => {
                (
                    event.name.includes(state.eventFilter) ||
                    [event.artist, event.knowArtist, event.otherArtist]
                        .flat()
                        .filter(artist => artist !== undefined)
                        .map(artist => artist.name).includes(state.eventFilter) ||
                    event.venue_name.includes(state.eventFilter)
                )
            })
            : state.events
    ,
    getLocation: state => state.location,
    getDate: state => state.date.selected,
    getPrevMonday: state => state.date.prevMonday,
    getZone: state => state.zone,
    getSelectedZone: state => state.selectedZone,
    hasAllowedSpotify: state => state.spotify.id !== '',
    hasAllowedLastFM: state => state.lastfm.id !== '',
    spotifyID: state => state.spotify.id,
    getSpotifyCreatedPlaylists: state => state.spotify.createdPlaylists,
    getSpotifySubscribePlaylists: state => state.spotify.subscribedPlaylists,
    getSpotifyMatchedArtists: state => state.spotify.matchedArtists,
    getSpotifyMatchedArtistsCount: state => state.spotify.matchedArtists.length,
    spotifyArtistCount: state => state.spotify.count,
    lastfmArtistCount: state => state.lastfm.count,
    featureArtist: state => state.artist.feature,
    artistInfo: state => state.artist.info,
    artist: state => state.artist.selected,
    artistEventFilterCoordinates: state => (state.artist.info.events) ? uniqBy(state.artist.info.events, "name").filter(event => event.lat !== null) : undefined,
    artistEventByDate: state => groupBy(uniqBy(state.artist.info.events, 'name'), "date"),
    artistEventSelected: state => state.artist.eventSelected,
    artistEventLastSelected: state => state.artist.eventLastSelected
}


export default new Vuex.Store({
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions
})
