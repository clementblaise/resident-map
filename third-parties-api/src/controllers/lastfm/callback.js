const log = require('../../utils').logService('callback [controller-lasftm]')
const axios = require('axios')
const md5 = require('md5')
const crypto = require('../../utils/crypto')
const neo4j = require('neo4j-driver').v1
const driver = require('../../db/driver')

if (process.env.NODE_ENV !== 'production') require('dotenv').config()


const findUserExist = "MATCH (user:LastFMUser {id: $id}) RETURN count(user)=1 as user_exists"
const updateUser = [
    'MATCH (user:LastFMUser {id: $id})',
    'SET user.key = $key',
].join('\n')
const addUser = [
    'MERGE (user:LastFMUser {id: $id})',
    'SET user.key = $key',
].join('\n')


async function saveUser(user) {
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session
        .run(findUserExist, {id: user.id})
        .then(result => result.records[0].get('user_exists'))
        .then(exist => {
            log.debug('Saving LasfFM session')
            if (exist) return session.run(updateUser, user)
            else return session.run(addUser, user)
        })
        .catch(err => log.error(err))

    session.close()

    return user

}


async function callback(req, res) {

    log.debug('Creating a new LasfFM session')

    log.debug(req.query)

    let token = req.query['token']

    let session = await new Promise(resolve => {
        axios.get('https://ws.audioscrobbler.com/2.0/?method=auth.getSession&format=json' +
            '&token=' + token +
            '&api_key=' + process.env.LASTFM_API_KEY.trim() +
            '&api_sig=' + md5('api_key' + process.env.LASTFM_API_KEY.trim() + 'methodauth.getSessiontoken' + token + process.env.LASTFM_API_SECRET.trim())
        )
            .then(result => resolve(result.data.session))
            .catch(err => {
                resolve({})
                log.error(err)
            })
    })

    let user = {
        id: session.name,
        key: crypto.encrypt(session.key),
    }

    await saveUser(user)

    res.redirect('https://residentmap.ml/#/lastfm/callback/' + crypto.encrypt(user.id))

}

module.exports = callback
