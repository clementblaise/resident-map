const log = require('../../utils').logService('token [controller-lastfm]')
const neo4j = require('neo4j-driver').v1
const driver = require('../../db/driver')
const crypto = require('../../utils/crypto')
const neo = require('../../utils/neo')

const getUser = "MATCH (user:LastFMUser {id: $id}) RETURN user"


async function getKey(id) {
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session
        .run(getUser, {id: id.trim()})
        .then(async result => {
            if (result.records.length === 0) return null
            return result.records[0].get('user').properties.key
        })
    session.close()
    return crypto.decrypt(await tx)
}

module.exports = {getKey}
