const log = require('../../utils').logService('sync [controller-spotify]')
const neo4j = require('neo4j-driver').v1
const driver = require('../../db/driver')
const LastFm = require('lastfm-node-client')

const updateArtistRelateUser = [
    'MATCH (user:LastFMUser {id: $id})',
    'UNWIND {list} as artist',
    'MATCH (a:Artist {name: artist})',
    'MERGE (user)-[:LISTEN_TO]->(a)'
].join('\n')


async function saveUserRelateArtist(user, artist) {
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session.run(updateArtistRelateUser, {list: artist, id: user}).catch(err => log.error(err))
    session.close()
    log.debug(`Created ${tx.summary.counters.relationshipsCreated()} LISTEN_TO relations`)
    return tx.summary.counters.relationshipsCreated()
}

function lastFMTopArtists(acc, lastFm, user, page) {
    return new Promise((resolve, reject) => {
        lastFm.userGetTopArtists({
            user: user,
            period: 'overall',
            limit: 1000,
            page: page
        })
            .then(result => {
                acc = [...acc, ...result.topartists.artist.map(artist => artist.name)]
                if (page === Number(result.topartists['@attr'].totalPages)) resolve(acc)
                else resolve(lastFMTopArtists(acc, lastFm, user, page + 1))
            })
            .catch(err => {
                log.error(err)
                reject()
            })
    })

}

async function analyseLastFMTopArtists(req, res) {
    const lastFm = new LastFm(process.env.LASTFM_API_KEY, process.env.LASTFM_API_SECRET)

    log.debug("Starting Anyalyse for use ", req.params.id)

    lastFMTopArtists([], lastFm, req.params.id, 1)
        .then(async artists => res.send({
            added: await saveUserRelateArtist(req.params.id, artists)
        }))

}

module.exports = analyseLastFMTopArtists
