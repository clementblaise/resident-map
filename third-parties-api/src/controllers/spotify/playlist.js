const spotifyApi = require('../../utils/spotifyApi')


function getUserPlaylist(req, res) {
    spotifyApi.setAccessToken(req.params.access)
    spotifyApi.getUserPlaylists(req.params.id, {limit: 50, offset: 0})
        .then(data => {
            let created = data.body.items.filter(playlist => playlist.owner.display_name === id)
            let subscribed = data.body.items.filter(playlist => playlist.owner.display_name !== id)
            res.send({created: created, subscribed: subscribed})
        })
}

module.exports = {getUserPlaylist}