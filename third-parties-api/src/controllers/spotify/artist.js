const log = require('../../utils').logService('artist [controller]')
const spotifyApi = require('../../utils/spotifyApi')
const neo4j = require('neo4j-driver').v1
const driver = require('../../db/driver')
const _ = require('lodash')


const updateArtistRelateUser = [
    'MATCH (user:SpotifyUser {id: $id})',
    'UNWIND {list} as artist',
    'MATCH (a:Artist {name: artist})',
    'MERGE (user)-[:LISTEN_TO]->(a)'
].join('\n')

function spotifyArtistsInSavedTracks(acc, access, offset) {
    spotifyApi.setAccessToken(access)
    return new Promise((resolve, reject) => {
        spotifyApi.getMySavedTracks({limit: 50, offset: offset})
            .then(data => {
                let artists = _.flatten(data.body.items.map(item => item.track.artists.map(artist => artist.name)))
                acc = _.flatten([...acc, artists])
                if (data.body.next != null) resolve(spotifyArtistsInSavedTracks(acc, access, offset + 50))
                else resolve(_.uniq(acc))
            })
            .catch(err => {
                log.error(err)
                reject()
            })
    })
}

async function saveUserRelateArtist(user, artist) {
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session.run(updateArtistRelateUser, {list: artist, id: user}).catch(err => log.error(err))
    session.close()
    log.debug(`Created ${tx.summary.counters.relationshipsCreated()} LISTEN_TO relations`)
    return tx.summary.counters.relationshipsCreated()
}


async function analyseSpotifySavedTracks(req, res) {
    log.debug("Querying saved tracks for ", req.params.id)
    spotifyArtistsInSavedTracks([], req.params.access, 0)
        .then(async artists => res.send({
            added: await saveUserRelateArtist(req.params.id, artists)
        }))
}

module.exports = {analyseSpotifySavedTracks}
