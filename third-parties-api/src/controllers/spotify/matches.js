const log = require('../../utils').logService('matches [controller]')
const neo4j = require('neo4j-driver').v1
const driver = require('../../db/driver')

const cypherUserRelateArtist = [
    'MATCH (user:SpotifyUser {id: $id})-[:LISTEN_TO]->(a:Artist)' +
    'RETURN a.name as artist',
].join('\n')

async function findUserRelateArtist(req, res) {
    let session = await driver.session(neo4j.session.READ)
    let tx = await session.run(cypherUserRelateArtist, {id: req.params.id}).catch(err => log.error(err))
    session.close()
    res.send(tx.records.map(record => record.get('artist')))
}


const findArtistRelateUserCount = [
    'MATCH (user:SpotifyUser {id: $id})-[:LISTEN_TO]->(a:Artist)' +
    'RETURN count(a) as artists_count',
].join('\n')


async function findUserRelateArtistCount(id) {
    let session = await driver.session(neo4j.session.READ)
    let tx = await session.run(findArtistRelateUserCount, {id: id}).catch(err => log.error(err))
    session.close()
    let count = neo.nodeToJSON(tx, 'artists_count')[0].artists_count.low
    log.debug(`Found ${count} artists related to user ${id}`)
    return count
}

// TODO : Move those the regular GraphQL API, we are not making spotify call
