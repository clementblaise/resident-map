const log = require('../../utils').logService('callback [controller-spotify]')
const spotifyApi = require('../../utils/spotifyApi')
const crypto = require('../../utils/crypto')
const neo4j = require('neo4j-driver').v1
const driver = require('../../db/driver')


const findUserExist = "MATCH (user:SpotifyUser {id: $id}) RETURN count(user)=1 as user_exists"
const updateUser = [
    'MATCH (user:SpotifyUser {id: $id})',
    'SET user.access = $access',
    'SET user.refresh = $refresh',
    'SET user.expires_at = $expires_at'
].join('\n')
const addUser = [
    'MERGE (user:SpotifyUser {id: $id})',
    'SET user.access = $access',
    'SET user.refresh = $refresh',
    'SET user.expires_at = $expires_at'
].join('\n')


async function saveUser(user) {
    user = {...user, expires_at: new neo4j.types.DateTime.fromStandardDate(new Date(user.expires_at))}
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session
        .run(findUserExist, {id: user.id})
        .then(result => result.records[0].get('user_exists'))
        .then(exist => {
            log.debug('Saving spotify access')
            if (exist) return session.run(updateUser, user)
            else return session.run(addUser, user)
        })
        .catch(err => log.error(err))

    session.close()

    return user

}

async function callback(req, res) {

    log.debug('Creating a new Spotify access')

    log.debug(req.query.code)

    let {code} = req.query;

    let data = await spotifyApi.authorizationCodeGrant(code).catch(err => log.error("authorizationCodeGrant ", err))

    let accessToken = data.body['access_token']
    let refreshToken = data.body['refresh_token']
    let expiresIn = data.body['expires_in']
    let now = new Date()
    let expiresAt = now.setSeconds(now.getSeconds() + Number(expiresIn))

    spotifyApi.setAccessToken(accessToken)
    spotifyApi.setRefreshToken(refreshToken)

    let userData = await spotifyApi.getMe().catch(err => log.error("getMe ", err))

    let userID = userData.body['id']

    let user = {
        id: userID,
        access: crypto.encrypt(accessToken),
        refresh: crypto.encrypt(refreshToken),
        expires_at: expiresAt
    }

    await saveUser(user)

    res.redirect('https://residentmap.ml/#/spotify/callback/' + crypto.encrypt(userID))
}

module.exports = callback
