const log = require('../../utils').logService('getToken')
const neo4j = require('neo4j-driver').v1
const driver = require('../../db/driver')
const crypto = require('../../utils/crypto')
const neo = require('../../utils/neo')
const spotifyApi = require('../../utils/spotifyApi')

const getUser = "MATCH (user:SpotifyUser {id: $id}) RETURN user.access as access, user.refresh as refresh, user.expires_at as expires"
const updateAccess = [
    'MATCH (user:SpotifyUser {id: $id})',
    'SET user.access = $access',
    'SET user.expires_at = $expires_at',
    'RETURN user.access as access'
].join('\n')


async function generateAccessToken(id) {
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session
        .run(getUser, {id: id.trim()})
        .then(async result => {
            if (result.records.length === 0) return null
            let user = neo.nodeToJSON(result, 'access', 'refresh', 'expires')[0]
            user = {...user, expires: new Date(user.expires)}
            // TODO : add a bit of delay for the next call
            if (new Date > user.expires) {
                spotifyApi.setRefreshToken(crypto.decrypt(user.refresh))
                let data = await spotifyApi.refreshAccessToken().catch(err => log.error('Refresh access ', err))
                log.info("Refresh token for ", id)
                let access = data.body['access_token']
                let expiresIn = data.body['expires_in']
                let now = new Date()
                let expiresAt = new neo4j.types.DateTime.fromStandardDate(new Date(now.setSeconds(now.getSeconds() + Number(expiresIn))))
                return session.run(updateAccess, {id: id, access: crypto.encrypt(access), expires_at: expiresAt})
            } else return result
        })
    session.close()
    return crypto.decrypt(neo.nodeToJSON(tx, 'access')[0].access)
}

module.exports = {generateAccessToken}
