module.exports = (app) => {

    const {createMiddleware} = require('@promster/express')
    let options = {
        accuracies: ['ms'],
        metricTypes: ['httpRequestsTotal', 'httpRequestsSummary', 'httpRequestsHistogram']
    }
    app.use(createMiddleware({app, options}))
    const {getSummary, getContentType} = require('@promster/express')

    app.use('/metrics', async (req, res) => {
        req.statusCode = 200
        res.setHeader('Content-Type', getContentType())
        res.send(getSummary())
    })

}
