module.exports = (app) => {

    const middleware = require('../midlleware')

    const callbackL = require('../controllers/lastfm/callback')
    const sync = require('../controllers/lastfm/sync')

    const callbackS = require('../controllers/spotify/callback')
    const playlist = require('../controllers/spotify/playlist')
    const artists = require('../controllers/spotify/artist')
    const axios = require('axios')

    app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        res.setHeader('Access-Control-Allow-Headers', 'Authorization, Content-Type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next()
    });

    app.get('/callbackspotify', (req, res) => callbackS(req, res))
    app.get('/callbacklastfm', (req, res) => callbackL(req, res))


    app.get('/analyse/:id', middleware.injectIDAndTokenSpotify, (req, res) => artists.analyseSpotifySavedTracks(req, res))
    app.get('/playlists/:id', middleware.injectIDAndTokenSpotify, (req, res) => playlist.getUserPlaylist(req, res))

    app.get('/sync/:id', middleware.injectIDLastFM, (req, res) => sync(req, res))

    app.get('/location', (req, res) => {
        axios.get('http://geoplugin.net/json.gp?ip=' + req.headers['x-original-forwarded-for'])
            .then(({data}) => res.send(data))

    })

}

// TODO : When syncing user artists create node if they don't exist
