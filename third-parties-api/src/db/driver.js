const neo4j = require('neo4j-driver').v1

if (process.env.NODE_ENV !== 'production') require('dotenv').config()
const host = "bolt://" + process.env.NEO4J_HOST.trim() + ":7687"
const username = process.env.NEO4J_USERNAME.trim()
const password = process.env.NEO4J_PASSWORD.trim()
const driver = neo4j.driver(host, neo4j.auth.basic(username, password))

module.exports = driver