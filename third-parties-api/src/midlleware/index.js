const spotifyToken = require('../controllers/spotify/token')
const lastfmToken = require('../controllers/lastfm/token')
const crypto = require('../utils/crypto')


async function injectIDAndTokenSpotify(req, res, next) {
    let id = crypto.decrypt(req.params.id)
    if (id == null) res.statusCode(401).send("Incorrect spotify id")
    else req.params.id = id

    req.params.access = await spotifyToken.generateAccessToken(req.params.id)

    next()
}

async function injectIDLastFM(req, res, next) {
    let id = crypto.decrypt(req.params.id)
    if (id == null) res.statusCode(401).send("Incorrect spotify id")
    else req.params.id = id

    req.params.key = await lastfmToken.getKey(req.params.id)

    next()
}

module.exports = {injectIDAndTokenSpotify, injectIDLastFM}
