const SpotifyWebApi = require('spotify-web-api-node');

if (process.env.NODE_ENV !== 'production') require('dotenv').config()

const spotifyApi = new SpotifyWebApi({
    clientId: process.env.SPOTIFY_API_ID.trim(),
    clientSecret: process.env.SPOTIFY_API_SECRET.trim(),
    redirectUri: 'https://thirdparties.residentmap.ml/callbackspotify'
});

module.exports = spotifyApi