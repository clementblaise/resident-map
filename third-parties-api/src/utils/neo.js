const _ = require('lodash')

function nodeToJSON(result, ...field) {
    let withProperties = result.records.map(record => _.flatten(field.map(field => {
        return {[field]: record.get(field)}
    })))
    return withProperties.map(props => props.reduce((a, b) => Object.assign({...a, ...b}), []))

}

module.exports = {nodeToJSON}
