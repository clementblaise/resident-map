const crypto = require('crypto')

if (process.env.NODE_ENV !== 'production') require('dotenv').config()

const ENCRYPTION_KEY = process.env.ENCRYPTION_KEY.trim()
const IV_LENGTH = 16

function encrypt(text) {
    let base64 = Buffer.from(text).toString('base64')
    let iv = crypto.randomBytes(IV_LENGTH)
    let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv)
    let encrypted = cipher.update(base64)

    encrypted = Buffer.concat([encrypted, cipher.final()])

    let salted = iv.toString('hex') + ':' + encrypted.toString('hex')
    return Buffer.from(salted).toString('base64')
}

function decrypt(base64) {
    let text = Buffer.from(base64, 'base64').toString()
    let textParts = text.split(':')
    let iv = Buffer.from(textParts.shift(), 'hex')
    let encryptedText = Buffer.from(textParts.join(':'), 'hex')

    try {
        let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv)
        let decrypted = decipher.update(encryptedText)
        decrypted = Buffer.concat([decrypted, decipher.final()])
        return Buffer.from(decrypted.toString(), 'base64').toString()
    } catch (err) {
        return null
    }

}

module.exports = {decrypt, encrypt}

