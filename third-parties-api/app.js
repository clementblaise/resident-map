const service = 'app'
const config = require('./src/config')
const log = require('./src/utils').logService(service)
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// Load middleware
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Load routes
require('./src/routes/prometheus')(app);
require('./src/routes/api')(app);

server = app.listen(config.port, () => log.info('Serving on port ' + config.port))
