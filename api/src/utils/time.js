const moment = require('moment')

function daysInWeek(day) {
    console.log(day)
    let date = moment(day)
    let start = date.clone().startOf('isoWeek')
    return Array(...Array(7)).map((_, i) => moment(start).add(i, 'days').format("YYYY-MM-DD"))
}


module.exports = {daysInWeek}
