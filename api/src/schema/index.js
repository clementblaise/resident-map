const graphql = require('graphql-tools')


const typeDefs = `

type Event {
    name: String!
    participant: Float
    address: String!
    lat: Float
    lon: Float
    dateStart: String!
    dateEnd: String!
    lasfmArtist: [String]
    spotifyArtist: [String]
    otherArtist: [String]
    venueName: String
    flyerPath: String
    artist: [String]
}

type EventArtist {
    name: String!
    participant: Float
    address: String!
    lat: Float
    lon: Float
    dateStart: String!
    dateEnd: String!
    venueName: String
    flyerPath: String
    area: String
}

type Artist {
    img: String!
    events: [EventArtist]
}


type Area {
    name: String!
    fullName: String!
}

type SavedArtist {
    count: Int
}

type EventsSummary {
    name: String!
    venueName: String
    dateStart: String!
}

type FeatureArtist {
    name: String!
    img: String!
    events: Int!
    rank: Int!
}

type RecommendedFeatureArtist {
    all: [FeatureArtist]
    lastfm: [FeatureArtist]
    spotify: [FeatureArtist]
}

type Zone {
    country: String
    isoCode: String
    area: [Area]
}

type SearchResult {
    name: String!
    type: String!
    area: String
    date: String
}

type Query {
  events(spotifyID: String, lastfmID: String, area: String!, date: String!, d0: String, d1: String, d2: String, d3: String, d4: String, d5: String, d6: String ): [Event]
  zone(country: String, code: String): [Zone]
  search(text: String!): [SearchResult]
  locate(lat: Float!, lon: Float!):[Area]
  spotify(spotifyID: String):[SavedArtist]
  lastfm(lastfmID: String):[SavedArtist]
  featureArtist(area: String!, spotifyID: String, lastfmID: String):RecommendedFeatureArtist
  eventArtist(name: String!):Artist
}

`;

// TODO : Add resolver to get and set tracked spotify playlist


const events = require('../resolvers/events')
const zone = require('../resolvers/zone')
const featureArtist = require('../resolvers/featureArtist')
const eventArtist = require('../resolvers/eventArtist')
const search = require('../resolvers/search')
const locate = require('../resolvers/locate')
const spotify = require('../resolvers/spotify')
const lastfm = require('../resolvers/lastfm')

const resolvers = {
    Query: {
        events: async (root, args, context) => await events(root, args, context),
        zone: (root, args, context) => zone(root, args, context),
        search: (root, args, context) => search(root, args, context),
        locate: (root, args, context) => locate(root, args, context),
        spotify: (root, args, context) => spotify(root, args, context),
        lastfm: (root, args, context) => lastfm(root, args, context),
        featureArtist: async (root, args, context) => await featureArtist(root, args, context),
        eventArtist: (root, args, context) => eventArtist(root, args, context),

    }
}


const schema = graphql.makeExecutableSchema({typeDefs, resolvers});


module.exports = schema
