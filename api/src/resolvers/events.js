const crypto = require('../utils/crypto')
const time = require('../utils/time')

async function events(root, args, context) {

    let dates = time.daysInWeek(args.date)

    args.d0 = dates[0]
    args.d1 = dates[1]
    args.d2 = dates[2]
    args.d3 = dates[3]
    args.d4 = dates[4]
    args.d5 = dates[5]
    args.d6 = dates[6]

    let session = context.driver.session();
    args.spotifyID = (args.spotifyID) ? crypto.decrypt(args.spotifyID) : undefined
    args.lastfmID = (args.lastfmID) ? crypto.decrypt(args.lastfmID) : undefined
    let events = (args.spotifyID && args.lastfmID) ? await eventSpotifyAndLastFM(root, args, session) : []
    events = (args.spotifyID && !args.lastfmID) ? [...events, ...await eventSpotify(root, args, session)] : [...events]
    events = (args.lastfmID && !args.spotifyID) ? [...events, ...await eventLastFM(root, args, session)] : [...events]
    let regularEvents = await event(root, args, session)
    console.log(events.length)
    console.log(regularEvents.length)
    events = (events.length > 0) ? [...events, ...regularEvents.filter(rEvent => events.findIndex(event => event.name === rEvent.name) === -1)] : regularEvents
    return events
}


module.exports = events

function eventSpotifyAndLastFM(root, args, session) {
    return session.run(
        ['MATCH (area:Area)',
            'WHERE area.name = $area',
            'MATCH (suser:SpotifyUser)',
            'WHERE suser.id = $spotifyID',
            'MATCH (luser:LastFMUser)',
            'WHERE luser.id = $lastfmID',
            'MATCH (suser)-[:LISTEN_TO]->(spotifyArtist:Artist)-[:PLAY_AT]->(event:Event)-[:AT_ADDRESS]->(addr:Address)-[:IN_AREA]->(area)',
            'MATCH (luser)-[:LISTEN_TO]->(lasfmArtist:Artist)-[:PLAY_AT]->(event:Event)-[:AT_ADDRESS]->(addr:Address)-[:IN_AREA]->(area)',
            "WHERE event.dateStart STARTS WITH $d0  OR event.dateStart STARTS WITH $d1 OR event.dateStart STARTS WITH $d2 OR event.dateStart STARTS WITH $d3 OR event.dateStart STARTS WITH $d4 OR event.dateStart STARTS WITH $d5 OR event.dateStart STARTS WITH $d6 ",
            'MATCH (event)<-[:PLAY_AT]-(allArtist:Artist)',
            'WITH collect( distinct spotifyArtist) as spotifyArtist, collect(distinct lasfmArtist) as lasfmArtist, collect(distinct allArtist) as allArtist, event, addr',
            'WITH filter(artist IN spotifyArtist WHERE NOT artist IN lasfmArtist) as spotifyArtist, lasfmArtist, allArtist, event, addr',
            'WITH filter(artist IN allArtist WHERE NOT artist IN lasfmArtist) as otherArtist , spotifyArtist, lasfmArtist, event, addr',
            'ORDER BY length(lasfmArtist) DESC, length(spotifyArtist) DESC, event.participant DESC, event.name DESC',
            'return otherArtist, spotifyArtist, lasfmArtist, event, addr'
        ].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    name: record.get("event").properties.name,
                    participant: record.get("event").properties.participant,
                    address: record.get("event").properties.address,
                    lat: record.get("addr").properties.lat,
                    lon: record.get("addr").properties.lon,
                    venueName: record.get("event").properties.venueName,
                    flyerPath: record.get("event").properties.flyerPath,
                    dateStart: record.get("event").properties.dateStart,
                    dateEnd: record.get("event").properties.dateEnd,
                    lasfmArtist: record.get("lasfmArtist").map(artist => artist.properties.name),
                    spotifyArtist: record.get("spotifyArtist").map(artist => artist.properties.name),
                    otherArtist: record.get("otherArtist").map(artist => artist.properties.name)
                }
            })
        })
}

function eventSpotify(root, args, session) {
    return session.run(
        ['MATCH (area:Area)',
            'WHERE area.name = $area',
            'MATCH (suser:SpotifyUser)',
            'WHERE suser.id = $spotifyID',
            'MATCH (suser)-[:LISTEN_TO]->(spotifyArtist:Artist)-[:PLAY_AT]->(event:Event)-[:AT_ADDRESS]->(addr:Address)-[:IN_AREA]->(area)',
            "WHERE event.dateStart STARTS WITH $d0 OR event.dateStart STARTS WITH $d1 OR event.dateStart STARTS WITH $d2 OR event.dateStart STARTS WITH $d3 OR event.dateStart STARTS WITH $d4 OR event.dateStart STARTS WITH $d5 OR event.dateStart STARTS WITH $d6 ",
            'MATCH (event)<-[:PLAY_AT]-(allArtist:Artist)',
            'WITH collect(distinct spotifyArtist) as spotifyArtist, collect(distinct allArtist) as allArtist, event, addr',
            'WITH filter(artist IN allArtist WHERE NOT artist IN spotifyArtist) as otherArtist , spotifyArtist, event, addr',
            'ORDER BY length(spotifyArtist) DESC, event.participant DESC, event.name DESC',
            'return otherArtist, spotifyArtist, event, addr'
        ].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    name: record.get("event").properties.name,
                    participant: record.get("event").properties.participant,
                    address: record.get("event").properties.address,
                    lat: record.get("addr").properties.lat,
                    lon: record.get("addr").properties.lon,
                    venueName: record.get("event").properties.venueName,
                    flyerPath: record.get("event").properties.flyerPath,
                    dateStart: record.get("event").properties.dateStart,
                    dateEnd: record.get("event").properties.dateEnd,
                    lasfmArtist: [],
                    spotifyArtist: record.get("spotifyArtist").map(artist => artist.properties.name),
                    otherArtist: record.get("otherArtist").map(artist => artist.properties.name)
                }
            })
        })
}

function eventLastFM(root, args, session) {
    return session.run(
        ['MATCH (area:Area)',
            'WHERE area.name = $area',
            'MATCH (luser:LastFMUser)',
            'WHERE luser.id = $lastfmID',
            'MATCH (luser)-[:LISTEN_TO]->(lasfmArtist:Artist)-[:PLAY_AT]->(event:Event)-[:AT_ADDRESS]->(addr:Address)-[:IN_AREA]->(area)',
            "WHERE event.dateStart STARTS WITH $d0  OR event.dateStart STARTS WITH $d1 OR event.dateStart STARTS WITH $d2 OR event.dateStart STARTS WITH $d3 OR event.dateStart STARTS WITH $d4 OR event.dateStart STARTS WITH $d5 OR event.dateStart STARTS WITH $d6 ",
            'MATCH (event)<-[:PLAY_AT]-(allArtist:Artist)',
            'WITH collect(distinct lasfmArtist) as lasfmArtist, collect(distinct allArtist) as allArtist, event, addr',
            'WITH filter(artist IN allArtist WHERE NOT artist IN lasfmArtist) as otherArtist, lasfmArtist, event, addr',
            'ORDER BY length(lasfmArtist) DESC, event.participant DESC, event.name DESC',
            'return otherArtist, lasfmArtist, event, addr'
        ].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    name: record.get("event").properties.name,
                    participant: record.get("event").properties.participant,
                    address: record.get("event").properties.address,
                    lat: record.get("addr").properties.lat,
                    lon: record.get("addr").properties.lon,
                    venueName: record.get("event").properties.venueName,
                    flyerPath: record.get("event").properties.flyerPath,
                    dateStart: record.get("event").properties.dateStart,
                    dateEnd: record.get("event").properties.dateEnd,
                    lasfmArtist: record.get("lasfmArtist").map(artist => artist.properties.name),
                    spotifyArtist: [],
                    otherArtist: record.get("otherArtist").map(artist => artist.properties.name)
                }
            })
        })
}

function event(root, args, session) {
    return session.run(
        ["MATCH (area:Area)",
            "WHERE area.name = $area",
            "MATCH (artist:Artist)-[:PLAY_AT]->(event:Event)-[:AT_ADDRESS]->(addr:Address)-[:IN_AREA]->(area)",
            //"ORDER BY event.participant, event.name DESC",
            "WHERE event.dateStart STARTS WITH $d0  OR event.dateStart STARTS WITH $d1 OR event.dateStart STARTS WITH $d2 OR event.dateStart STARTS WITH $d3 OR event.dateStart STARTS WITH $d4 OR event.dateStart STARTS WITH $d5 OR event.dateStart STARTS WITH $d6 ",
            "RETURN event, collect(distinct artist) as artist, addr"
        ].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    name: record.get("event").properties.name,
                    participant: record.get("event").properties.participant,
                    address: record.get("event").properties.address,
                    lat: record.get("addr").properties.lat,
                    lon: record.get("addr").properties.lon,
                    dateStart: record.get("event").properties.dateStart,
                    dateEnd: record.get("event").properties.dateEnd,
                    venueName: record.get("event").properties.venueName,
                    flyerPath: record.get("event").properties.flyerPath,
                    lasfmArtist: [],
                    spotifyArtist: [],
                    otherArtist: record.get("artist").map(artist => artist.properties.name),
                }
            })
        })
}
