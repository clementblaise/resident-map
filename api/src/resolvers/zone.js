function zone(root, args, context) {

    let session = context.driver.session();
    let query

    if (args.country) query = ["MATCH (country:Country)",
        "WHERE country.name = $country",
        "MATCH (area:Area)-[:IN_COUNTRY]->(country)",
        "RETURN country, collect(distinct area) as area"
    ].join('\n')

    else if (args.code) query = ["MATCH (country:Country)",
        "WHERE country.code = $code",
        "MATCH (area:Area)-[:IN_COUNTRY]->(country)",
        "RETURN country, collect(distinct area) as area"
    ].join('\n')

    else query = ["MATCH (area:Area)-[:IN_COUNTRY]->(country:Country)",
            "RETURN country, collect(distinct area) as area"
        ].join('\n')

    return session.run(
        query
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    country: record.get("country").properties.name,
                    isoCode: record.get("country").properties.isoCode,
                    area: record.get("area").map(area => {
                        return {
                            name: area.properties.area,
                            fullName: area.properties.name
                        }
                    })
                }
            })
        })
}

module.exports = zone
