const _ = require('lodash')

function searchArtist(root, args, session) {
    return session.run(
        ["match (artist:Artist)",
            `WHERE toLower(artist.name) CONTAINS toLower($text) OR ANY(word IN FILTER(x IN SPLIT({text}, " ") WHERE x <> '')`,
            "   WHERE toLower(artist.name) CONTAINS toLower(word))",
            "RETURN distinct artist",
            "LIMIT 20",
        ].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    name: record.get("artist").properties.name,
                    type: "artist",
                    area: null,
                    date: null
                }
            })
        })
}

function searchEvents(root, args, session) {
    return session.run(
        ["match (event:Event)",
            `WHERE toLower(event.name) CONTAINS toLower($text) OR ANY(word IN FILTER(x IN SPLIT({text}, " ") WHERE x <> '')`,
            "   WHERE toLower(event.name) CONTAINS toLower(word))",
            "MATCH (event)-[:AT_ADDRESS]->(:Address)-[:IN_AREA]->(area:Area)",
            "RETURN distinct event, area",
            "LIMIT 20",
        ].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    name: record.get("event").properties.name,
                    type: "event",
                    area: record.get("area").properties.name,
                    date: record.get("event").properties.dateStart
                }
            })
        })
}


async function search(root, args, context) {
    let result = {events: [], artists: []}
    if (args.text === '' || args.text.length < 1) {
        return result
    }
    let session1 = context.driver.session()
    let session2 = context.driver.session()
    return Promise.all([searchArtist(root, args, session1), searchEvents(root, args, session2)])
        .then(result => _.flatten(result))
}

module.exports = search
