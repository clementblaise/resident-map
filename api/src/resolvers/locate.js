function locate(root, args, context) {
    let session = context.driver.session()
    return session.run(
        ["MATCH (add:Address)",
            "WITH toFloat(add.lat) as addlat, toFloat(add.lon) as addlon, add",
            "WITH 2 * 6371 * asin(sqrt(haversin(radians($lat - addlat))+ cos(radians($lat))* cos(radians(addlat))* haversin(radians($lon - addlon)))) as distance, add",
            "ORDER BY distance",
            "LIMIT 20",
            "MATCH (add)-[:IN_AREA]->(area:Area)",
            "WITH count(area) as count, area",
            "ORDER BY count DESC",
            "return count, area"].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    name: record.get("area").properties.area,
                    fullName: record.get("area").properties.name
                }
            })
        })
}

module.exports = locate
