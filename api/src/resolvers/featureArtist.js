const crypto = require('../utils/crypto')
const moment = require('moment')


async function featureArtist(root, args, context) {
    let date = moment(new Date())
    args.day = date.date()
    args.month = date.month() + 1
    args.year = date.year()

    args.spotifyID = (args.spotifyID) ? crypto.decrypt(args.spotifyID) : undefined
    args.lastfmID = (args.lastfmID) ? crypto.decrypt(args.lastfmID) : undefined

    let session = context.driver.session()

    if (args.lastfmID && args.spotifyID) {
        let all = await featureAllArtist(root, args, session)
        let lastfm = await featureLastFMArtist(root, args, session)
        let spotify = await featureSpotifyArtist(root, args, session)
        all = all.filter(artist => spotify.findIndex(spotifyArtist => spotifyArtist.name === artist.name) || lastfm.findIndex(lastfmArtist => lastfmArtist.name === artist.name))
        spotify = spotify.filter(artist => lastfm.findIndex(lastfmArtist => lastfmArtist.name === artist.name))
        return {all: all, lastfm: lastfm, spotify: spotify}
    } else if (args.spotifyID) {
        let all = await featureAllArtist(root, args, session)
        let spotify = await featureSpotifyArtist(root, args, session)
        return {
            all: all.filter(artist => spotify.findIndex(spotifyArtist => spotifyArtist.name === artist.name)),
            spotify: spotify,
            lastfm: null
        }
    } else if (args.lastfmID) {
        let all = await featureAllArtist(root, args, session)
        let lastfm = await featureLastFMArtist(root, args, session)
        return {
            all: all.filter(artist => lastfm.findIndex(lastfmArtist => lastfmArtist.name === artist.name)),
            lastfm: lastfm,
            spotify: null
        }
    } else {
        let all = await featureAllArtist(root, args, session)
        return {all: all, spotify: null, lastfm: null}
    }
}

module.exports = featureArtist


function featureAllArtist(root, args, session) {
    return session.run(
        ["MATCH (a:Artist)-[:PLAY_AT]->(e:Event)-[:AT_ADDRESS]->(:Address)-[:IN_AREA]->(:Area {name: $area})",
            "WITH split(e.dateStart, ' ') as datetimeSplit, a, e",
            "WITH [item in split(datetimeSplit[0], '-') | toInteger(item)] as dateSplit, a, e",
            "WITH date({day: dateSplit[2], month: dateSplit[1], year: dateSplit[0]}) as date, a, e",
            "WHERE date > date({day: toInt($day), month: toInt($month), year: toInt($year)})",
            "WITH collect(e) as events, count(e) as countEvent, a, date",
            "MATCH (a)-[:PLAY_AT]->(allEvent:Event)",
            "WITH count(allEvent) as totalCount, countEvent, a, date",
            "ORDER BY totalCount DESC",
            "RETURN distinct a, countEvent, totalCount"].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    name: record.get("a").properties.name,
                    img: "images/profiles/lg/" + record.get("a").properties.raPath.replace('/dj/', '') + '.jpg',
                    events: record.get("countEvent").low,
                    rank: record.get("totalCount").low,
                }
            })
        })
}

function featureSpotifyArtist(root, args, session) {
    return session.run(
        ["MATCH (:SpotifyUser {id: $spotifyID})-[:LISTEN_TO]->(a:Artist)-[:PLAY_AT]->(e:Event)-[:AT_ADDRESS]->(:Address)-[:IN_AREA]->(:Area {name: $area})",
            "WITH split(e.dateStart, ' ') as datetimeSplit, a, e",
            "WITH [item in split(datetimeSplit[0], '-') | toInteger(item)] as dateSplit, a, e",
            "WITH date({day: dateSplit[2], month: dateSplit[1], year: dateSplit[0]}) as date, a, e",
            "WHERE date > date({day: toInt($day), month: toInt($month), year: toInt($year)})",
            "WITH collect(e) as events, count(e) as countEvent, a, date",
            "MATCH (a)-[:PLAY_AT]->(allEvent:Event)",
            "WITH count(allEvent) as totalCount, countEvent, a, date",
            "ORDER BY totalCount DESC",
            "RETURN distinct a, countEvent, totalCount"].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    name: record.get("a").properties.name,
                    img: "images/profiles/lg/" + record.get("a").properties.raPath.replace('/dj/', '') + '.jpg',
                    events: record.get("countEvent").low,
                    rank: record.get("totalCount").low,
                }
            })
        })
}

function featureLastFMArtist(root, args, session) {
    return session.run(
        ["MATCH (:LastFMUser {id: $lastfmID})-[:LISTEN_TO]->(a:Artist)-[:PLAY_AT]->(e:Event)-[:AT_ADDRESS]->(:Address)-[:IN_AREA]->(:Area {name: $area})",
            "WITH split(e.dateStart, ' ') as datetimeSplit, a, e",
            "WITH [item in split(datetimeSplit[0], '-') | toInteger(item)] as dateSplit, a, e",
            "WITH date({day: dateSplit[2], month: dateSplit[1], year: dateSplit[0]}) as date, a, e",
            "WHERE date > date({day: toInt($day), month: toInt($month), year: toInt($year)})",
            "WITH collect(e) as events, count(e) as countEvent, a, date",
            "MATCH (a)-[:PLAY_AT]->(allEvent:Event)",
            "WITH count(allEvent) as totalCount, countEvent, a, date",
            "ORDER BY totalCount DESC",
            "RETURN distinct a, countEvent, totalCount"].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    name: record.get("a").properties.name,
                    img: "images/profiles/lg/" + record.get("a").properties.raPath.replace('/dj/', '') + '.jpg',
                    events: record.get("countEvent").low,
                    rank: record.get("totalCount").low,
                }
            })
        })
}
