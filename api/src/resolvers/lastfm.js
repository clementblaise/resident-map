const crypto = require('../utils/crypto')


function lastfm(root, args, context) {
    args.lastfmID = (args.lastfmID) ? crypto.decrypt(args.lastfmID) : undefined
    let session = context.driver.session()
    return session.run(
        ["match (u:LastFMUser{ id: $lastfmID})-[:LISTEN_TO]->(a:Artist)", "return count(a) as count"].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    count: record.get("count").low,
                }
            })
        })
}

module.exports = lastfm
