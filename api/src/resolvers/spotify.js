const crypto = require('../utils/crypto')


function spotify(root, args, context) {
    args.spotifyID = (args.spotifyID) ? crypto.decrypt(args.spotifyID) : undefined
    let session = context.driver.session()
    return session.run(
        ["match (u:SpotifyUser{ id: $spotifyID})-[:LISTEN_TO]->(a:Artist)", "return count(a) as count"].join('\n')
        , args)
        .then(result => {
            return result.records.map(record => {
                return {
                    count: record.get("count").low,
                }
            })
        })
}

module.exports = spotify
