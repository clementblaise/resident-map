function eventArtist(root, args, context) {
    let session = context.driver.session()
    return session.run(
        ["MATCH (a:Artist {name: $name})-[:PLAY_AT]->(e:Event)-[:AT_ADDRESS]->(addr:Address)-[:IN_AREA]->(area:Area)",
            "WITH e.dateStart as date, e, a, addr, area",
            "ORDER BY date DESC",
            "LIMIT 200",
            "RETURN a as artist, e as event, addr, area"].join('\n')
        , args)
        .then(result => {
            return {
                img: "images/profiles/lg/" + result.records[0].get("artist").properties.raPath.replace('/dj/', '') + '.jpg',
                events: result.records.map(record => {
                    return {
                        name: record.get("event").properties.name,
                        participant: record.get("event").properties.participant,
                        address: record.get("event").properties.address,
                        lat: record.get("addr").properties.lat,
                        lon: record.get("addr").properties.lon,
                        dateStart: record.get("event").properties.dateStart,
                        dateEnd: record.get("event").properties.dateEnd,
                        venueName: record.get("event").properties.venueName,
                        flyerPath: record.get("event").properties.flyerPath,
                        area: record.get("area").properties.name
                    }
                })
            }
        })
}

module.exports = eventArtist
