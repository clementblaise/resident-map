const service = 'app'
const config = require('./src/config')
const log = require('./src/utils').logService(service)
const neo4j = require('neo4j-driver')
const ApolloServer = require('apollo-server').ApolloServer

if (process.env.NODE_ENV !== 'production') require('dotenv').config()
const host = "bolt://" + process.env.NEO4J_HOST.trim() + ":7687"
const username = process.env.NEO4J_USERNAME.trim()
const password = process.env.NEO4J_PASSWORD.trim()
const driver = neo4j.driver(host, neo4j.auth.basic(username, password))

const schema = require('./src/schema')

const server = new ApolloServer({schema, context: {driver}});

server.listen(config.port, '0.0.0.0').then(({url}) => {
    console.log(`GraphQL API ready at ${url}`);
});
