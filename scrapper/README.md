# resident-map-scraper

## Project setup
```
yarn install
```

## Run the application
```
yarn start
```

## Run tests
```
yarn test
```

### Configuration

#### Geocoder

You need to set two variables for the geocoding service :
* GEOCODER_NAME="name" ie: Mapquest, OpenCage or Google
* GEOCODER_KEY="YOUR_API_KEY"

#### Limited scraping for dev environment
In development mode we limit the country which are scrape, you can change which country are scrape the _config_ file ,
refer to [resident advisor region](https://www.residentadvisor.net/events?show=all) to find which are available.

If you want to scrape all the country, overwrite the _NODE_ENV_ variable inside the _.env_ file with something else than __dev__

#### Concurrency limit

In order to avoid rate limiting from resident advisor HTTP call are limited to 5 simultaneously, it also help if you are using neo4j on a laptop as the database might have some trouble keeping up with the import
You can change the value at your own risk, it's in the _config_ file, look at the log of you see some warnings, it's too high ;)