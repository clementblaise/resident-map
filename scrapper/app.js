const log = require('./src/utils').logService('main')
const ingest = require('./src/service/ingester')
const config = require('./src/config')
const {loadEnv} = require('./src/utils/env')

const main = async () => {
    await loadEnv()

    if (process.env.NODE_ENV === 'dev') {
        await ingest.eventOnCountry(process.env.WEEK_START, process.env.WEEK_END, config.devCountries)
    } else {
        await ingest.event(process.env.WEEK_START, process.env.WEEK_END)
    }

    process.exit(0)

}


main()

