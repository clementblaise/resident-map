const service = 'country-scrapper'
const config = require('../../config')
const axios = require('axios')
const cheerio = require('cheerio')
const md5 = require('md5')
const log = require('../../utils').logService(service)
const warnRequest = require('../../utils').warnRequest
const countriesISOCode = require('../../assets/iso-countries')

/**
 * Scrape DOM and return countries as Array[Object]
 * @param {Function} rawPage
 * @returns {Array.<Object>}
 */
function scrapeCountries(rawPage) {
    let countries = []

    rawPage('ul[class=ulRegions]').find('li').each((index, value) => {

        if (rawPage(value).find('img').length === 1) {

            let name = rawPage(value).find('img').attr('alt')
            let flagPath = rawPage(value).find('img').attr('src')

            countries = [...countries, {
                hash: md5(name),
                name: name,
                flagPath: flagPath,
                isoCode: countryNameToISOCode(name)
            }]
        }
    })

    return countries
}

/**
 * Make HTTP request and return countries
 * @returns {Promise<Array><Object>}
 */
function requestCountries() {
    return new Promise((resolve, reject) => {

        axios.get(config.ra + '/events?show=all')

            .then(response => {
                let countries = scrapeCountries(cheerio.load(response.data))
                log.info(`Retrieved ${countries.length} countries`)
                resolve(countries)
            })

            .catch(err => {
                warnRequest(log, 'all', 'country', err)
                reject(err)
            })
    })
}

function countryNameToISOCode(countryName) {
    return countriesISOCode.filter(country => country.name === countryName)[0].code
}

module.exports = {
    scrapeCountries,
    requestCountries,
}
