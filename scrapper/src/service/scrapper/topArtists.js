const service = 'areas-scrapper'
const config = require('../../config')
const axios = require('axios')
const cheerio = require('cheerio')
const md5 = require('md5')
const log = require('../../utils').logService(service)
const warnRequest = require('../../utils').warnRequest


function getTopArtist() {
    return new Promise((resolve, reject) => {
        axios.get(ra + '/dj.aspx')
            .then(response => {
                let rawPage = cheerio.load(response.data)
                let topArtists = []

                rawPage('div[class=plus8]').find('a').each((index, value) => {
                    topArtists.push(rawPage(value).attr('href'))
                })

                resolve(topArtists
                    .filter(artist => artist !== undefined)
                    .map(artist => artist.match('\/dj\/(?<name>.*)'))
                    .filter(artist => artist != null)
                    .map(artist => artist.groups.name)
                )
            })
            .catch(err => reject(err))
    })
}