const {requestCountries} = require('./countries')
const {requestAreas} = require('./areas')
const {requestEvents} = require('./events')
const {requestEventDetails} = require('./eventsDetails')

module.exports = {
    countries: requestCountries,
    area: requestAreas,
    events: requestEvents,
    eventDetails: requestEventDetails
}
