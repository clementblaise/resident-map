const service = 'eventsDetails-scrapper'
const config = require('../../config')
const cheerio = require('cheerio')
const moment = require('moment')
const _ = require('lodash')
const md5 = require('md5')
const log = require('../../utils').logService(service)
const warnRequest = require('../../utils').warnRequest
const axios = require('../../utils/axiosBounded')


/**
 * Parse date, start time and end time to a start and end date with time
 * @param {string} day - Formatted as 31-12-2019
 * @param {string} start - Formatted as 22:00
 * @param {string} end - Formatted as 10:00
 * @returns {{start: , end: *}|{start: null, end: null}}
 */
function parseDate(day, start, end) {
    // TODO: Make date timezone aware
    let dayStart, dayEnd

    // Check if there are different dates (for festival)
    let splitDay = day.split(" ")
    if (splitDay.length === 3) {
        dayStart = [splitDay[0], splitDay[1], splitDay[2]].join('-')
        dayEnd = [splitDay[0], splitDay[1], splitDay[2]].join('-')
    } else if (splitDay.length === 6) {
        dayStart = [splitDay[0], splitDay[1], splitDay[2]].join('-')
        dayEnd = [splitDay[3], splitDay[4], splitDay[5]].join('-')
    }

    // Parse the date to moment object
    let dateStart = moment(dayStart, 'DD-MMM-YYYY', 'en')
    let dateEnd = moment(dayEnd, 'DD-MMM-YYYY', 'en')

    // Parse time
    let splitStartTime = start.split(':')
    let splitEndTime = end.split(':')
    let startHour = splitStartTime[0]
    let startMinute = splitStartTime[1]
    let endHour = splitEndTime[0]
    let endMinute = splitEndTime[1]

    // Check if end if the next day
    if (splitDay.length === 3 && moment(end, 'hh:mm').isBefore(moment(start, 'hh:mm'))) {
        dateEnd.add(1, 'days').hours(endHour).minutes(endMinute)
    } else dateEnd.hours(endHour).minutes(endMinute)

    dateStart.hours(startHour).minutes(startMinute)


    // Check that we got a valid date
    if (dateStart.isValid() && dateEnd.isValid()) return {
        start: dateStart.format('YYYY-MM-DD HH:mm:ss'),
        end: dateEnd.format('YYYY-MM-DD HH:mm:ss')
    }
    else {
        log.error(`Parsing date : ${day} ${start} ${end}`)
        return {start: new Date(1970, 1, 1), end: new Date(1970, 1, 1)}
    }

}


/**
 * Scrape DOM and return enriched details with dates and venue
 * @param {Function} rawPage
 * @param {Object} details
 * @returns {Object} details
 */
function scrapeInfo(rawPage, details) {

    // TODO : Remove ":" at the beginning of event name

    rawPage('aside[id=detail]').find('li').each((index, value) => {
        if (rawPage(value).find('div').text() === "Date /") {
            rawPage(value).find('div').remove()
            let date = rawPage(value).find('a').text()
            rawPage(value).find('a').remove()
            let time = rawPage(value).html().split('<br><br>')[1]
            if (time === undefined) time = rawPage(value).html().split('<br>')[1]

            let begin = time.split('-')[0].trim()
            let end = time.split('-')[1].trim()
            let parsedDate = parseDate(date, begin, end)

            details = {
                ...details,
                dateStart: parsedDate.start,
                dateEnd: parsedDate.end
            }
        }

        if (rawPage(value).find('div').text() === "Venue /") {
            let venuePath = rawPage(value)
                .find('a')
                .attr('href')
                .match('id=(?<id>.*)')
            details = (venuePath != null) ? {...details, venueId: venuePath.groups.id} : details

            let venueName = cheerio.load(value)('a[class=cat-rev]').text()
            //details = (venueName === '') ? {...details, venue_name: undefined} : {...details, venue_name: venueName}
            details = (venueName !== '') ? {...details, venueName: venueName} : details

            rawPage(value).find('div').remove()
            details.address = rawPage(value).text()

            details = {
                ...details,
                name: details.name.replace(details.venueName, "").trim(),
                address: details.address.replace(details.venueName, "").trim()
            }

            details = {
                ...details,
                addressHash: md5(details.address),
                name: (details.name.charAt(0) === ':') ? details.name.substring(1) : details.name
            }
        }
    })

    return details

}

/**
 * Scrape DOM and return enriched details with artist
 * @param {Function} rawPage
 * @param {Object} details
 * @returns {Object} details
 */
function scrapeArtist(rawPage, details) {
    let artist = []
    rawPage('div[id=event-item]').find('p').find('a').each((index, value) => {
        if (rawPage(value).text() === '[email protected]') return

        artist = [...artist, {
            name: rawPage(value).text(),
            raPath: rawPage(value).attr('href'),
            hash: md5(rawPage(value).text())
        }]
    })

    return {...details, artists: artist}
}

/**
 * Scrape DOM and return enriched details with flyer path
 * @param {Function} rawPage
 * @param {Object} details
 * @returns {Object} details
 */
function scrapeFlyer(rawPage, details) {
    let flyerPath = rawPage('div[class=flyer]').find('img').attr('src')
    details = (flyerPath !== undefined) ? {...details, flyerPath: flyerPath} : details
    return details
}

function scrapeParticipant(rawPage, details) {
    let participant = Number(rawPage('h1[id=MembersFavouriteCount]').text().trim())
    return {...details, participant: participant}
}

/**
 * Make HTTP request and return events details
 * @param {Object} event
 * @returns {Promise<Array><Object>}
 */
function requestEventDetails(event) {
    return new Promise(resolve => {
        if (event.length === 0) return resolve({})
        axios.get(event.path)

            .then(response => {
                let rawPage = cheerio.load(response.data)
                let details = {name: event.name, hash: event.path.replace('/events/', ''), area: event.area}
                details = scrapeInfo(rawPage, details)
                details = scrapeArtist(rawPage, details)
                details = scrapeFlyer(rawPage, details)
                details = scrapeParticipant(rawPage, details)
                log.debug(`Retrieved details for event ${event.path}`)
                resolve(details)
            })

            .catch(err => {
                warnRequest(log, event.path, 'event details', err)
                resolve({})
            })
    })
}

module.exports = {
    scrapeInfo,
    scrapeArtist,
    requestEventDetails,
    scrapeParticipant,
    scrapeFlyer
}
