const service = 'events-scrapper'
const config = require('../../config')
const cheerio = require('cheerio')
const moment = require('moment')
const _ = require('lodash')
const md5 = require('md5')
const log = require('../../utils').logService(service)
const warnRequest = require('../../utils').warnRequest
const axios = require('../../utils/axiosBounded')


/**
 * Get monday date from relative week in the past, present and future
 * @param {number} week - relative week integer ex: 0 is this week, -1 is past the week, 1 is next week
 * @returns {string} date - relative monday date formatted as '2019-12-31'
 */
function relativeMonday(week) {
    moment.locale('fr-ca')
    return moment().isoWeekday(-7 + ((week + 1) * 7) + 1).format('L')
}

/**
 *  Generates an array of monday date based on relative value
 *  ex [0, 0] is this monday's week
 *     [-1, 1] is last, current and next weeks monday's
 * @param {number} weekStart
 * @param {number} weekEnd
 * @returns {Array<string>} - Array of dates
 */
function queryDates(weekStart, weekEnd) {

    let relativeWeeks = []
    for (curr = Number(weekStart); curr <= Number(weekEnd); curr++) {
        relativeWeeks = [...relativeWeeks, curr]
    }

    return relativeWeeks.map(week => relativeMonday(week))
}

/**
 * Scrape DOM and return countries
 * @param {Function} rawPage
 * @param {Object} area
 * @returns {Array.<Object>}
 */
function scrapeEvents(rawPage, area) {
    let events = []

    rawPage('div[id=event-listing]').find('li').each((index, value) => {
        if (rawPage(value).find('article').length !== 1) return

        events = [...events, {
            name: cheerio.load(value)('a[itemprop=url]').text(),
            path: cheerio.load(value)('a[itemprop=url]').attr('href'),
            area: area.hash
        }]
    })

    return events
}

/**
 * Make HTTP request and return events
 * @param {number} weekStart
 * @param {number} weekEnd
 * @param {Object} area
 * @returns {Promise<Array><Object>}
 */
async function requestEvents(weekStart, weekEnd, area) {
    let dates = queryDates(weekStart, weekEnd)

    return Promise.all(dates.map(date => {
        return new Promise(resolve => {

            axios.get(area.path + "/week/" + date)

                .then(response => {
                    let events = scrapeEvents(cheerio.load(response.data), area)
                    log.info(`Retrieved ${events.length} events for ${area.path} on ${date}`)
                    resolve(events)
                })

                .catch(err => {
                    warnRequest(log, area.path, 'event', err)
                    resolve([])
                })
        })
    }))

}

module.exports = {
    scrapeEvents,
    requestEvents
}
