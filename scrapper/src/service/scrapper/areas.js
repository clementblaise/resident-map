const service = 'areas-scrapper'
const config = require('../../config')
const cheerio = require('cheerio')
const md5 = require('md5')
const log = require('../../utils').logService(service)
const warnRequest = require('../../utils').warnRequest

const axios = require('../../utils/axiosBounded')

/**
 * Scrape DOM and return areas
 * @param {Function} rawPage
 * @returns {Array.<Object>}
 */
function scrapeArea(rawPage) {
    let areas = []

    rawPage('ul[class=ulRegions]').find('li').each((index, value) => {

        if (rawPage(value).find('img').length === 1) return     // img are only for country
        let area = rawPage(value).find('a').attr('title').split(',')[0]
        let country = rawPage(value).find('a').attr('title').split(',')[1].trim()
        let path = rawPage(value).find('a').attr('href')


        areas = [...areas, {
            hash: md5(country + ' - ' + area),
            name: country + ' - ' + area,
            area: area,
            country: country,
            path: path
        }]
    })

    return areas
}

/**
 * Make HTTP request and return areas
 * @returns {Promise<Array><Object>}
 */
function requestAreas() {
    return new Promise((resolve, reject) => {

        axios.get('/events?show=all')

            .then(response => {
                let areas = scrapeArea(cheerio.load(response.data))
                log.info(`Retrieved ${areas.length} areas`)
                resolve(areas)
            })

            .catch(err => {
                warnRequest(log, 'all', 'area', err)
                reject(err)
            })
    })
}

module.exports = {
    requestAreas
}