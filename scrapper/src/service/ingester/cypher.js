const cypher = {}

cypher.countryNode = [
    'UNWIND {list} as row',
    'MERGE (c:Country {id: row.id})',
    'SET c += row.properties'
].join('\n')

cypher.areaNode = [
    'UNWIND {list} as row',
    'MERGE (a:Area {id: row.id})',
    'SET a += row.properties'
].join('\n')

cypher.areaRelateCountry = [
    'UNWIND {list} as row',
    'MATCH (a:Area {id: row.area}), (c:Country {name: row.country})',
    'MERGE (a)-[:IN_COUNTRY]->(c)'
].join('\n')

cypher.artistNode = [
    'UNWIND {list} as row',
    'MERGE (a:Artist {id: row.id})',
    'SET a += row.properties'
].join('\n')


cypher.findAddreess = [
    'UNWIND {list} as row',
    'MATCH (a:Address {id: row.id})',
    'RETURN a.id as hash'
].join('\n')

cypher.createAddress = [
    'UNWIND {list} as row',
    'MERGE (a:Address {id: row.id})',
    'SET a += row.properties',
].join('\n')

cypher.eventNode = [
    'UNWIND {list} as row',
    'MERGE (e:Event {id: row.id})',
    'SET e += row.properties'
].join('\n')

cypher.artistRelateEvent = [
    'UNWIND {list} as row',
    'MATCH (e:Event {id: row.event}), (a:Artist {id: row.artist})',
    'MERGE (e)<-[:PLAY_AT]-(a)'
].join('\n')

cypher.eventsRelateAreaAndAddress = [
    'UNWIND {list} as row',
    'MATCH (e:Event {id: row.event}), (add:Address {id: row.addressHash}), (area:Area {id: row.area})',
    'MERGE (e)-[:AT_ADDRESS]->(add)-[:IN_AREA]->(area)'

].join('\n')

cypher.cleanRelationship = [
    "start r=relationship(*)",
    "match (s)-[r]->(e)",
    "with s,e,type(r) as typ, tail(collect(r)) as coll",
    "foreach(x in coll | delete x)"
].join('\n')

module.exports = cypher
