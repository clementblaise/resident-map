const _ = require('lodash')
const log = require('../../utils').logService('ingester')
const RA = require('../scrapper')
const neo4j = require('neo4j-driver').v1
const cypher = require('./cypher')
const geocoder = require('../../utils/geocoder')
const axios = require('axios')
const md5 = require('md5')
const osm = require('../../utils/axiosRateLimited')

if (process.env.NODE_ENV !== 'production') require('dotenv').config()
const host = "bolt://" + process.env.NEO4J_HOST.trim() + ":7687"
const username = process.env.NEO4J_USERNAME.trim()
const password = process.env.NEO4J_PASSWORD.trim()
const driver = neo4j.driver(host, neo4j.auth.basic(username, password))


/**
 * Send country nodes to neo4j
 * @param countries
 * @returns {Promise<*>}
 */
async function createCountryNode(countries) {
    let nodes = countries.map(country => {
        let {hash, ...properties} = country
        return {id: hash, properties: properties}
    })
    log.debug(`Sending ${nodes.length} Country nodes`)
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session.run(cypher.countryNode, {list: nodes}).catch(err => log.error(err))
    session.close()
    log.info(`Created ${tx.summary.counters.nodesCreated()} Country nodes`)
    return countries
}

/**
 * Send area nodes to neo4j
 * @param areas
 * @returns {Promise<*>}
 */
async function createAreaNode(areas) {
    let nodes = areas.map(({country, ...area}) => area)
    nodes = nodes.map(area => {
        let {hash, ...properties} = area
        return {id: hash, properties: properties}
    })
    log.debug(`Sending ${nodes.length} Area nodes`)
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session.run(cypher.areaNode, {list: nodes}).catch(err => log.error(err))
    session.close()
    log.info(`Created ${tx.summary.counters.nodesCreated()} Area nodes`)
    return areas
}

/**
 * Send IN_COUNTRY relations to neo4j
 * @param areas
 * @returns {Promise<*>}
 */
async function updateAreaRelateCountry(areas) {
    let relations = areas.map(area => {
        return {area: area.hash, country: area.country}
    })
    log.debug(`Sending ${relations.length} IN_COUNTRY relations`)
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session.run(cypher.areaRelateCountry, {list: relations}).catch(err => log.error(err))
    session.close()
    log.info(`Created ${tx.summary.counters.relationshipsCreated()} IN_COUNTRY relations`)
    return areas
}

/**
 * Send artist nodes to neo4j
 * @param events
 * @returns {Promise<*>}
 */
async function createArtistNode(events) {
    let nodes = _.uniqBy(_.flatten(events.map(event => event.artists)), 'name')
    nodes = nodes.filter(node => node)
    nodes = nodes.map(artist => {
        let {hash, ...properties} = artist
        return {id: hash, properties: properties}
    })
    log.debug(`Sending ${nodes.length} Artist nodes`)
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session.run(cypher.artistNode, {list: nodes}).catch(err => log.error(err))
    session.close()
    log.info(`Created ${tx.summary.counters.nodesCreated()} Artist nodes`)
    return events
}

/**
 * Send event nodes to neo4j
 * @param events
 * @returns {Promise<*>}
 */
async function createEventNode(events) {
    // TODO : Add constraint on event name, verify if date should be updated (festival)
    let nodes = events.map(({artists, ...event}) => event)
    nodes = nodes.map(event => {
        let {hash, ...properties} = event
        return {id: hash, properties: properties}
    })
    log.debug(`Sending ${nodes.length} Event nodes`)
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session.run(cypher.eventNode, {list: nodes}).catch(err => log.error(err))
    session.close()
    log.info(`Created ${tx.summary.counters.nodesCreated()} Event nodes with property ${tx.summary.counters.propertiesSet()} set`)
    return events
}

/**
 * Send IN_AREA relations to neo4j
 * @param events
 * @returns {Promise<*>}
 */
async function createEventsRelateAreaAndAddress(events) {
    let relations = events.map(event => {
        return {event: event.hash, area: event.area, addressHash: event.addressHash}
    })
    log.debug(`Sending ${relations.length} IN_AREA relations`)
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session.run(cypher.eventsRelateAreaAndAddress, {list: relations}).catch(err => log.error(err))
    session.close()
    log.info(`Created ${tx.summary.counters.relationshipsCreated()} IN_AREA relations`)
    return events
}

/**
 * Send PLAY_AT relations to neo4j
 * @param events
 * @returns {Promise<*>}
 */
async function createArtistRelateEvent(events) {
    let relations = _.flatten(events.map(event => event.artists.map(artist => {
        return {event: event.hash, artist: artist.hash}
    })))
    let session = await driver.session(neo4j.session.WRITE)
    log.debug(`Sending ${relations.length} PLAY_AT relations`)
    let tx = await session.run(cypher.artistRelateEvent, {list: relations}).catch(err => log.error(err))
    session.close()
    log.info(`Created ${tx.summary.counters.relationshipsCreated()} PLAY_AT relations`)
    driver.close()
    return events
}


//
// 4372c1d06780c8


async function geocode(address) {
    log.debug("Query geocode ", address)
    return new Promise(resolve => {
        osm
            .get('search.php?&q=' + encodeURIComponent(address) +
                '&format=json')
            .then(async response => {
                (response.data.length === 0) ? log.warn('Using fallback') : log.info('Found the address')
                let geo = (response.data.length === 0)
                    ? await geocodeFallBack(address)
                    : {latitude: response.data[0].lat, longitude: response.data[0].lon}
                resolve(geo)
            })
            .catch(async err => {
                log.error("Default geocoder : " + err)
                resolve(await geocodeFallBack(address))
            })
    })
}

async function geocodeFallBack(address) {

    return new Promise(resolve => {
        geocoder.geocode(address)
            .then(result => {
                if (result.length === 0) {
                    log.wanr('Fallback did not found the address')
                    resolve(undefined)
                } else {
                    log.info('Fallback found the address')
                    resolve({latitude: result[0].latitude, longitude: result[0].longitude})
                }
            })
            .catch(err => {
                log.error("Fallaback geocoder : " + err)
                resolve(undefined)
            })
    })

}


/**
 * Group all the address and filter
 * @param events
 * @returns {{addressesWithVenue: *, addressesTBA: *}}
 */
function addressParser(events) {
    let addresses = Object.keys(_.groupBy(events, 'address'))
    let addressesWithVenue = addresses
        .filter(address => !(
                address.includes('TBA') ||
                address.includes('tba') ||
                address.includes('secret') ||
                address.includes('Secret') ||
                address.includes('unknown')
            )
        )
    let addressesTBA =
        addresses
            .filter(address => (
                    address.includes('TBA') ||
                    address.includes('tba') ||
                    address.includes('secret') ||
                    address.includes('Secret') ||
                    address.includes('unknown')
                )
            )

    return {addressesWithVenue: addressesWithVenue, addressesTBA: addressesTBA}

}


async function addressWithoutGeocode(addresses) {
    addresses = addresses.map(address => {
        return {
            id: md5(address),
            name: address
        }
    })
    let session = await driver.session(neo4j.session.READ)
    let tx = await session.run(cypher.findAddreess, {list: addresses}).catch(err => log.error(err))
    session.close()
    let addressesWithGeocode = tx.records.map(record => record.get('hash'))
    return addresses.filter(address => !addressesWithGeocode.includes(address.id))
}

async function createAddressNode(addresses) {

    addresses = addresses.map(event => {
        let {id, ...properties} = event
        return {id: id, properties: properties}
    })

    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session.run(cypher.createAddress, {list: addresses}).catch(err => log.error(err))
    session.close()
    log.info(`Created ${tx.summary.counters.nodesCreated()} Address nodes`)
    return addresses

}

async function addressHandler(events) {
    let parsedAddress = addressParser(events)
    let addressesToGeocode = await addressWithoutGeocode(parsedAddress.addressesWithVenue)
    log.info(`${addressesToGeocode.length} addresses are going to be geocoded`)
    let addresses = await Promise.all(addressesToGeocode.map(async address => {
        let geo = await geocode(address.name)
        address.lon = geo.longitude
        address.lat = geo.latitude
        return address

    }))
    let addressUnGeocoded = addresses.filter(address => address.lon === undefined)
    if (addressUnGeocoded.length > 1) log.warn(`Could not geocode ${addressUnGeocoded.length} address : \n ${addressUnGeocoded}`)

    let tba = parsedAddress.addressesTBA.map(address => {
        return {
            id: md5(address),
            name: address
        }
    })

    return await createAddressNode([...addresses, ...tba])
}


async function cleanRelationship() {
    let session = await driver.session(neo4j.session.WRITE)
    let tx = await session.run(cypher.cleanRelationship).catch(err => log.error(err))
    session.close()
    log.info(`Cleaned ${tx.summary.counters.relationshipsDeleted()} relation`)
    return true
}

/**
 * Dispatch query for events
 * @param events
 * @returns {Promise<*>}
 */
async function eventHandler(events) {
    events = events.filter(event => event).filter(event => event.hash)
    await addressHandler(events)
    await createArtistNode(events)
    await createEventNode(events)
    await createEventsRelateAreaAndAddress(events)
    await createArtistRelateEvent(events)
    await cleanRelationship()
    return events
}

/**
 * Ingest countries
 * @returns {Promise<*>}
 */
const ingestCountries = () => RA.countries().then(countries => createCountryNode(countries))

/**
 * Dispatch query for areas
 * @param areas
 * @returns {Promise<*>}
 */
async function areaHanlder(areas) {
    await createAreaNode(areas)
    await updateAreaRelateCountry(areas)
    return areas
}

/**
 * Ingest areas
 * @returns {Promise<unknown>}
 */
const ingestArea = () => RA.area().then(areas => areaHanlder(areas))


/**
 * Ingest events
 * @param {Number} weekStart
 * @param {Number} weekEnd
 * @returns {Promise}
 */
const ingestEvent = (weekStart, weekEnd) => {
    return new Promise(resolve => {
        RA.area()
            .then(areas => areas.map(area => RA.events(weekStart, weekEnd, area)))
            .then(eventsPromise => _.flatten(eventsPromise))
            .then(eventsPromise => Promise.all(eventsPromise))
            .then(events => _.flattenDepth(events, 2))
            .then(events => events.map(event => RA.eventDetails(event)))
            .then(eventDetailsPromise => Promise.all(eventDetailsPromise))
            .then(events => eventHandler(events))
            .then(() => resolve())
    })
}


/**
 * Ingest events for selected countries
 * @param {Number} weekStart
 * @param {Number} weekEnd
 * @param {Array<string>} countries
 * @returns {Promise}
 */
const ingestEventOnCountry = (weekStart, weekEnd, countries) => {
    return new Promise(resolve => {
        RA.area()
            .then(areas => areas.filter(area => countries.includes(area.country)))
            .then(areas => areas.map(area => RA.events(weekStart, weekEnd, area)))
            .then(eventsPromise => _.flatten(eventsPromise))
            .then(eventsPromise => Promise.all(eventsPromise))
            .then(events => _.flattenDepth(events, 2))
            .then(events => events.map(event => RA.eventDetails(event)))
            .then(eventDetailsPromise => Promise.all(eventDetailsPromise))
            .then(events => eventHandler(events))
            .then(() => resolve())
    })
}


module.exports = {
    countries: ingestCountries,
    area: ingestArea,
    event: ingestEvent,
    eventOnCountry: ingestEventOnCountry
}
