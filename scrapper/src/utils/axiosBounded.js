const {ConcurrencyManager} = require("axios-concurrency");
const axios = require("axios");
const config = require('../config')

if (process.env.NODE_ENV !== 'production') require('dotenv').config()

let api = axios.create({
    baseURL: "https://www.residentadvisor.net"
})

const manager = ConcurrencyManager(api, process.env.MAX_CONCURRENT_REQUESTS);

module.exports = api