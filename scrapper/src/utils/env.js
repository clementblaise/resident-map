
exports.loadEnv = () => {

    let requiredEnv = [
        'NEO4J_HOST',
        'NEO4J_USERNAME',
        'NEO4J_PASSWORD',
        'WEEK_START',
        "WEEK_END",
        'MAX_CONCURRENT_REQUESTS',
        'GEOCODER_NAME',
        'GEOCODER_KEY'
    ]

    if (process.env.NODE_ENV !== 'production') {
        require('dotenv').config()
    }


    if (process.env.LOG_LEVEL === 'undefined') {
        process.env.LOG_LEVEL = (process.env.NODE_ENV !== 'production') ? 'WARN' : 'DEBUG'
    }

    let unsetEnv = requiredEnv.filter((env) => !(typeof process.env[env] !== 'undefined'));
    if (unsetEnv.length > 0) {
        throw  new Error("Required ENV variables are not set: [" + unsetEnv.join(', ') + "]");
    }

}
