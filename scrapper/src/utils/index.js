
/**
 *
 * @param {string} service
 * @returns {*|Logger|Logger|Logger}
 */
exports.logService = (service) => {
    const Logger = require('logplease');
    const logger = Logger.create(service);
    Logger.setLogLevel(process.env.LOG_LEVEL);
    return logger
}

/**
 * @param {*|Logger|Logger|Logger} log
 * @param {string} endpoint
 * @param {string} name
 * @param err
 */
exports.warnRequest = (log, endpoint, name, err) => {

    if (err.response !== undefined) {
        log.warn(`Could not retrieve ${name} for ${endpoint} due to HTTP ${err.response.status}`)
    } else {
        log.warn(`Could not retrieve ${name} for ${endpoint} : ${err}`)
    }
}


exports.neodeError = (log, action, type, node, err) => {
    if (err.code) {
        log.warn(`${action} ${type} node ${node} : ${err.code}`)
    } else {
        log.error(`${action} ${type} node ${node} : ${err}`)
    }
}



