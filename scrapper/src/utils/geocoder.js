require('dotenv').config()
const NodeGeocoder = require('node-geocoder');

const options = {
    provider: process.env.GEOCODER_NAME,
    httpAdapter: 'https',
    apiKey: process.env.GEOCODER_KEY,
    formatter: null           // 'gpx', 'string', ...
};

const geocoder = NodeGeocoder(options)

module.exports = geocoder