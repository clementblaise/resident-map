const axios = require('axios')
const rateLimit = require('axios-rate-limit')

let osm = rateLimit(
    axios.create({
        baseURL: "https://nominatim.openstreetmap.org/",
        headers: {'User-Agent': 'residentmap.ml'}
        }),
    { maxRequests: 1, perMilliseconds: 1000, maxRPS: 1 }
    )

module.exports = osm