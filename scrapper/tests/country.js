const expect = require('chai').expect
const { scrapeCountries } = require('../src/service/scrapper/countries')
const cheerio = require('cheerio')
const fs = require('fs')
const path = require('path');

const countries = require('./assets/country')

describe("Country", () => {

    let rawPage = cheerio.load(fs.readFileSync(path.join(__dirname, './assets/country/index.html')))

    it('should list of country', () => {

        let result = scrapeCountries(rawPage)
        expect(result).to.deep.equal(countries)
    })

})