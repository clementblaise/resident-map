
let expect = {}

expect.events = [
    {
        name: 'One More: Kristofo, Antony Adam, Rafaël Murillo, Papa est Maman',
        path: '/events/1349073',
        area: 'MD5HASH'
    },
    {
        name: 'Distrikt Paris 4 Years Birthday - Warehouse Part 2',
        path: '/events/1349832',
        area: 'MD5HASH'
    },
    {
        name: 'RAW: Under Black Helmet, Tommy Holohan, Fuerr, 1ndica',
        path: '/events/1344109',
        area: 'MD5HASH'
    },
    {
        name: 'Gabber Roots: New Year Party',
        path: '/events/1347700',
        area: 'MD5HASH'
    }
]

module.exports = expect