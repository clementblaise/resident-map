const country = [
    {
        hash: 'c420ddff824a5c0eec70dd23d62496bc',
        name: 'Albania',
        flag_path: 'https://www.residentadvisor.net/images/flags/AL.gif'
    },
    {
        hash: '575b9408b6daa2ddcefbcf6d81c9b4c9',
        name: 'Algeria',
        flag_path: 'https://www.residentadvisor.net/images/flags/DZ.gif'
    },
    {
        hash: '68746a7280b143cfc01f967610d3e86d',
        name: 'Andorra',
        flag_path: 'https://www.residentadvisor.net/images/flags/AD.gif'
    },
    {
        hash: '232bf11cb81bcdb269f76a08fde8b947',
        name: 'Angola',
        flag_path: 'https://www.residentadvisor.net/images/flags/AO.gif'
    },
    {
        hash: 'f7e68bf0791888ebcd5bfc62e022aa83',
        name: 'Antigua and Barbuda',
        flag_path: 'https://www.residentadvisor.net/images/flags/AG.gif'
    },
    {
        hash: '3536be57ce0713954e454ae6c53ec023',
        name: 'Argentina',
        flag_path: 'https://www.residentadvisor.net/images/flags/AR.gif'
    },
    {
        hash: 'a00c273f0f497484093fa94865cf5ca5',
        name: 'Armenia',
        flag_path: 'https://www.residentadvisor.net/images/flags/AM.gif'
    },
    {
        hash: '4442e4af0916f53a07fb8ca9a49b98ed',
        name: 'Australia',
        flag_path: 'https://www.residentadvisor.net/images/flags/AU.gif'
    },
    {
        hash: '9891739094756d2605946c867b32ad28',
        name: 'Austria',
        flag_path: 'https://www.residentadvisor.net/images/flags/AT.gif'
    },
    {
        hash: '7176f8c3bccfdf02322c73f7a0bc9688',
        name: 'Azerbaijan',
        flag_path: 'https://www.residentadvisor.net/images/flags/AZ.gif'
    },
    {
        hash: '6dbefdc38954fc54ea0c697d0c6ec0a7',
        name: 'Bahamas',
        flag_path: 'https://www.residentadvisor.net/images/flags/BS.gif'
    },
    {
        hash: '6ddecd8ccd9f648d19dc02c7a566cb4f',
        name: 'Bahrain',
        flag_path: 'https://www.residentadvisor.net/images/flags/BA.gif'
    },
    {
        hash: 'f78a77f631d275aac6a914a17fe1b885',
        name: 'Bangladesh',
        flag_path: 'https://www.residentadvisor.net/images/flags/BD.gif'
    },
    {
        hash: '3214c0f21d200a1dae4eb83a53ec2730',
        name: 'Barbados',
        flag_path: 'https://www.residentadvisor.net/images/flags/BB.gif'
    },
    {
        hash: '6542f875eaa09a5c550e5f3986400ad9',
        name: 'Belarus',
        flag_path: 'https://www.residentadvisor.net/images/flags/BY.gif'
    },
    {
        hash: '6c1674d14bf5f95742f572cddb0641a7',
        name: 'Belgium',
        flag_path: 'https://www.residentadvisor.net/images/flags/BE.gif'
    },
    {
        hash: '20bca6785240fa722edb5c85d055a93d',
        name: 'Belize',
        flag_path: 'https://www.residentadvisor.net/images/flags/BH.gif'
    },
    {
        hash: '9e7002d53d4db7bfad4f5cf419b0c126',
        name: 'Benin',
        flag_path: 'https://www.residentadvisor.net/images/flags/BJ.gif'
    },
    {
        hash: '6653bea16d7ac8a266d752e9a0176c4f',
        name: 'Bermuda',
        flag_path: 'https://www.residentadvisor.net/images/flags/BM.gif'
    },
    {
        hash: 'e65a0ed0e39dd14a9e5af441efb09b37',
        name: 'Bhutan',
        flag_path: 'https://www.residentadvisor.net/images/flags/BT.gif'
    },
    {
        hash: '28aaafc355915903ce4a7e1d3af3bc4e',
        name: 'Bolivia',
        flag_path: 'https://www.residentadvisor.net/images/flags/BO.gif'
    },
    {
        hash: '96a6dd711874d4323dc2d3f932bd2ed3',
        name: 'Bosnia and Herzegovina',
        flag_path: 'https://www.residentadvisor.net/images/flags/BIH.gif'
    },
    {
        hash: '6cd50fb3091b0a9d3c1ac2cf52441390',
        name: 'Botswana',
        flag_path: 'https://www.residentadvisor.net/images/flags/BW.gif'
    },
    {
        hash: '42537f0fb56e31e20ab9c2305752087d',
        name: 'Brazil',
        flag_path: 'https://www.residentadvisor.net/images/flags/BR.gif'
    },
    {
        hash: '01a151debf2bfee8906f43f4342eb10b',
        name: 'Bulgaria',
        flag_path: 'https://www.residentadvisor.net/images/flags/BG.gif'
    },
    {
        hash: '28814f84db09b59b95f863b9c143c3a9',
        name: 'Burkina Faso',
        flag_path: 'https://www.residentadvisor.net/images/flags/BF.gif'
    },
    {
        hash: '06c7a3c3a3344a62864b951aec4df830',
        name: 'Cambodia',
        flag_path: 'https://www.residentadvisor.net/images/flags/KH.gif'
    },
    {
        hash: '4be25f9d27da71d4e98775668b5b12af',
        name: 'Cameroon',
        flag_path: 'https://www.residentadvisor.net/images/flags/CM.gif'
    },
    {
        hash: '445d337b5cd5de476f99333df6b0c2a7',
        name: 'Canada',
        flag_path: 'https://www.residentadvisor.net/images/flags/CA.gif'
    },
    {
        hash: '6802039d41676bdd09823704b0b9ba92',
        name: 'Cayman Islands',
        flag_path: 'https://www.residentadvisor.net/images/flags/KY.gif'
    },
    {
        hash: '2e6507f70a9cc26fb50f5fd82a83c7ef',
        name: 'Chile',
        flag_path: 'https://www.residentadvisor.net/images/flags/CL.gif'
    },
    {
        hash: 'ae54a5c026f31ada088992587d92cb3a',
        name: 'China',
        flag_path: 'https://www.residentadvisor.net/images/flags/CN.gif'
    },
    {
        hash: 'ef3388cc5659bccb742fb8af762f1bfd',
        name: 'Colombia',
        flag_path: 'https://www.residentadvisor.net/images/flags/CO.gif'
    },
    {
        hash: '5882b568d8a010ef48a6896f53b6eddb',
        name: 'Costa Rica',
        flag_path: 'https://www.residentadvisor.net/images/flags/CR.gif'
    },
    {
        hash: '0bdd913223016e1283cf9a2b96d44b99',
        name: "Cote d'Ivoire",
        flag_path: 'https://www.residentadvisor.net/images/flags/CI.gif'
    },
    {
        hash: '560d4c6ff431c86546f3fcec72c748c7',
        name: 'Croatia',
        flag_path: 'https://www.residentadvisor.net/images/flags/HR.gif'
    },
    {
        hash: '33cac763789c407f405b2cf0dce7df89',
        name: 'Cuba',
        flag_path: 'https://www.residentadvisor.net/images/flags/CU.gif'
    },
    {
        hash: '0d2268d462cd08b20b7508c3fc0fde5e',
        name: 'Curacao',
        flag_path: 'https://www.residentadvisor.net/images/flags/CW.gif'
    },
    {
        hash: 'ea2ba3f8011e19e3101ce65fdcefbcc4',
        name: 'Cyprus',
        flag_path: 'https://www.residentadvisor.net/images/flags/CY.gif'
    },
    {
        hash: '51802d8bb965d0e5be697f07d16922e8',
        name: 'Czech Republic',
        flag_path: 'https://www.residentadvisor.net/images/flags/CZ.gif'
    },
    {
        hash: 'b2ce07e6bc0b55222aa66fcc1b16b732',
        name: 'Democratic Republic of the Congo',
        flag_path: 'https://www.residentadvisor.net/images/flags/CD.gif'
    },
    {
        hash: '424214945ba5615eca039bfe5d731c09',
        name: 'Denmark',
        flag_path: 'https://www.residentadvisor.net/images/flags/DK.gif'
    },
    {
        hash: '662c15622f9c9c5decc0db80edb6c416',
        name: 'Dominican Republic',
        flag_path: 'https://www.residentadvisor.net/images/flags/DO.gif'
    },
    {
        hash: '4d5d85af33ec2aaedb674d2d6a7d53b6',
        name: 'Ecuador',
        flag_path: 'https://www.residentadvisor.net/images/flags/EC.gif'
    },
    {
        hash: 'e31959fe2842dacea4d16d36e9813620',
        name: 'Egypt',
        flag_path: 'https://www.residentadvisor.net/images/flags/EG.gif'
    },
    {
        hash: 'e96d24bdfc024e04f49f1f0cc011ca20',
        name: 'El Salvador',
        flag_path: 'https://www.residentadvisor.net/images/flags/SV.gif'
    },
    {
        hash: '7755415a9fe7022060b98a689236ccd2',
        name: 'Estonia',
        flag_path: 'https://www.residentadvisor.net/images/flags/EE.gif'
    },
    {
        hash: 'e299d7cb0f7866cce7d90da2af14047c',
        name: 'Ethiopia',
        flag_path: 'https://www.residentadvisor.net/images/flags/ET.gif'
    },
    {
        hash: '55b0c4d4efa00b59643b2e6a6e7c18c0',
        name: 'Fiji',
        flag_path: 'https://www.residentadvisor.net/images/flags/FJ.gif'
    },
    {
        hash: '6f781c6559a0c605da918096bdb69edf',
        name: 'Finland',
        flag_path: 'https://www.residentadvisor.net/images/flags/FI.gif'
    },
    {
        hash: '0309a6c666a7a803fdb9db95de71cf01',
        name: 'France',
        flag_path: 'https://www.residentadvisor.net/images/flags/FR.gif'
    },
    {
        hash: 'e2a96e074e1d8a6f6de56abbd4a4d8dc',
        name: 'Gambia',
        flag_path: 'https://www.residentadvisor.net/images/flags/GM.gif'
    },
    {
        hash: 'eada819634d0164c6a7547bdcc405033',
        name: 'Georgia',
        flag_path: 'https://www.residentadvisor.net/images/flags/GE.gif'
    },
    {
        hash: 'd8b00929dec65d422303256336ada04f',
        name: 'Germany',
        flag_path: 'https://www.residentadvisor.net/images/flags/DE.gif'
    },
    {
        hash: 'e7400496f1ce70cb62c2c44ca2ddc469',
        name: 'Ghana',
        flag_path: 'https://www.residentadvisor.net/images/flags/GH.gif'
    },
    {
        hash: '672566a43483aa8212cb365658600b99',
        name: 'Gibraltar',
        flag_path: 'https://www.residentadvisor.net/images/flags/GI.gif'
    },
    {
        hash: '6b718641741f992e68ec3712718561b8',
        name: 'Greece',
        flag_path: 'https://www.residentadvisor.net/images/flags/GR.gif'
    },
    {
        hash: '2dc47f81fc4257e14c4d0fcd90d03b9a',
        name: 'Guam',
        flag_path: 'https://www.residentadvisor.net/images/flags/GU.gif'
    },
    {
        hash: '948b13d5a3e11e21baadc349e199020e',
        name: 'Guatemala',
        flag_path: 'https://www.residentadvisor.net/images/flags/GT.gif'
    },
    {
        hash: 'f4270ce39e7e926052e097a0e4e63bde',
        name: 'Honduras',
        flag_path: 'https://www.residentadvisor.net/images/flags/HN.gif'
    },
    {
        hash: 'fa79c3005daec47ecff84a116a0927a1',
        name: 'Hungary',
        flag_path: 'https://www.residentadvisor.net/images/flags/HU.gif'
    },
    {
        hash: 'b78edab0f52e0d6c195fd0d8c5709d26',
        name: 'Iceland',
        flag_path: 'https://www.residentadvisor.net/images/flags/IS.gif'
    },
    {
        hash: '7d31e0da1ab99fe8b08a22118e2f402b',
        name: 'India',
        flag_path: 'https://www.residentadvisor.net/images/flags/IN.gif'
    },
    {
        hash: '4647d00cf81f8fb0ab80f753320d0fc9',
        name: 'Indonesia',
        flag_path: 'https://www.residentadvisor.net/images/flags/ID.gif'
    },
    {
        hash: '21fc68909a9eb8692e84cf64e495213e',
        name: 'Iran',
        flag_path: 'https://www.residentadvisor.net/images/flags/IR.gif'
    },
    {
        hash: '06e415f918c577f07328a52e24f75d43',
        name: 'Ireland',
        flag_path: 'https://www.residentadvisor.net/images/flags/IE.gif'
    },
    {
        hash: '5a548c2f5875f10bf5614b7c258876cf',
        name: 'Israel',
        flag_path: 'https://www.residentadvisor.net/images/flags/IL.gif'
    },
    {
        hash: '1007e1b7f894dfbf72a0eaa80f3bc57e',
        name: 'Italy',
        flag_path: 'https://www.residentadvisor.net/images/flags/IT.gif'
    },
    {
        hash: '1add2eb41fcae9b2a15b4a7d68571409',
        name: 'Jamaica',
        flag_path: 'https://www.residentadvisor.net/images/flags/JM.gif'
    },
    {
        hash: '53a577bb3bc587b0c28ab808390f1c9b',
        name: 'Japan',
        flag_path: 'https://www.residentadvisor.net/images/flags/JP.gif'
    },
    {
        hash: '6ea1e24d60afddf388b06f8243c45b70',
        name: 'Jordan',
        flag_path: 'https://www.residentadvisor.net/images/flags/JO.gif'
    },
    {
        hash: '7c1a943bf29d2c753fb935e99428482c',
        name: 'Kazakhstan',
        flag_path: 'https://www.residentadvisor.net/images/flags/KZ.gif'
    },
    {
        hash: '94984a8c4896946d9bafd24959cb6181',
        name: 'Kenya',
        flag_path: 'https://www.residentadvisor.net/images/flags/KE.gif'
    },
    {
        hash: '40e5f4acf26ffcc0490082c11aad831f',
        name: 'Kosovo',
        flag_path: 'https://www.residentadvisor.net/images/flags/XK.gif'
    },
    {
        hash: '05387f3ca38d7bd84ae35f31f2899ecf',
        name: 'Kuwait',
        flag_path: 'https://www.residentadvisor.net/images/flags/KW.gif'
    },
    {
        hash: '601aebf4afb1db3fdb5c88f3cdda23ef',
        name: 'Laos',
        flag_path: 'https://www.residentadvisor.net/images/flags/LA.gif'
    },
    {
        hash: 'a09f4b2ae67f0a63ab8912047a1a1b55',
        name: 'Latvia',
        flag_path: 'https://www.residentadvisor.net/images/flags/LV.gif'
    },
    {
        hash: 'b45ff92cc522bb89bfc3b8ef3fc21b7d',
        name: 'Lebanon',
        flag_path: 'https://www.residentadvisor.net/images/flags/LB.gif'
    },
    {
        hash: 'd9051e0b77f8bb5521389618e70e2ada',
        name: 'Lithuania',
        flag_path: 'https://www.residentadvisor.net/images/flags/LT.gif'
    },
    {
        hash: '06630c890abadde9228ea818ce52b621',
        name: 'Luxembourg',
        flag_path: 'https://www.residentadvisor.net/images/flags/LU.gif'
    },
    {
        hash: '5547baeda33255ad8f5307fc92cb589e',
        name: 'Macedonia',
        flag_path: 'https://www.residentadvisor.net/images/flags/MK.gif'
    },
    {
        hash: 'b5bcce260d9e303ca0e63f055187ed28',
        name: 'Madagascar',
        flag_path: 'https://www.residentadvisor.net/images/flags/MG.gif'
    },
    {
        hash: '70965feb0441ff7fc1982fc5c509136e',
        name: 'Malawi',
        flag_path: 'https://www.residentadvisor.net/images/flags/MW.gif'
    },
    {
        hash: '3f0e49c46cbde0c7adf5ea04a97ab261',
        name: 'Malaysia',
        flag_path: 'https://www.residentadvisor.net/images/flags/MY.gif'
    },
    {
        hash: '62235142f3fca96e1f2cd0ed4a7de48d',
        name: 'Maldives',
        flag_path: 'https://www.residentadvisor.net/images/flags/MV.gif'
    },
    {
        hash: '92468e8a62373add2b9caefddbcf1303',
        name: 'Malta',
        flag_path: 'https://www.residentadvisor.net/images/flags/MT.gif'
    },
    {
        hash: '07f3ca235faaa1c9ad16facef5526d8b',
        name: 'Mauritius',
        flag_path: 'https://www.residentadvisor.net/images/flags/MU.gif'
    },
    {
        hash: '8dbb07a18d46f63d8b3c8994d5ccc351',
        name: 'Mexico',
        flag_path: 'https://www.residentadvisor.net/images/flags/MX.gif'
    },
    {
        hash: '0c12f5495afe76d9242ed25668979de9',
        name: 'Moldova',
        flag_path: 'https://www.residentadvisor.net/images/flags/MD.gif'
    },
    {
        hash: 'd6a297c6193fd59309453a0db7a51b90',
        name: 'Monaco',
        flag_path: 'https://www.residentadvisor.net/images/flags/MC.gif'
    },
    {
        hash: 'bb6a72b6a93150d4181e50496fc15f5a',
        name: 'Mongolia',
        flag_path: 'https://www.residentadvisor.net/images/flags/MN.gif'
    },
    {
        hash: '4e92f9d2cdf0b8eb493ae3a19709d121',
        name: 'Montenegro',
        flag_path: 'https://www.residentadvisor.net/images/flags/ME.gif'
    },
    {
        hash: '4d4a1722d8e85909a576da2c42878ff0',
        name: 'Morocco',
        flag_path: 'https://www.residentadvisor.net/images/flags/MA.gif'
    },
    {
        hash: 'a6d3bea3fa66775952e080b90bb0c4f1',
        name: 'Mozambique',
        flag_path: 'https://www.residentadvisor.net/images/flags/MZ.gif'
    },
    {
        hash: '217fa54cc9351504fcde32147bff005d',
        name: 'Myanmar',
        flag_path: 'https://www.residentadvisor.net/images/flags/MM.gif'
    },
    {
        hash: '7fef6b003c726890ea5ca3708fe8ff56',
        name: 'Nepal',
        flag_path: 'https://www.residentadvisor.net/images/flags/NP.gif'
    },
    {
        hash: 'a67d4cbdd1b59e0ffccc6bafc83eb033',
        name: 'Netherlands',
        flag_path: 'https://www.residentadvisor.net/images/flags/NL.gif'
    },
    {
        hash: 'c51ed580ea5e20c910d951f692512b4d',
        name: 'New Zealand',
        flag_path: 'https://www.residentadvisor.net/images/flags/NZ.gif'
    },
    {
        hash: '3bfe17f6c2d1b8941df303de7aec2eb0',
        name: 'Nicaragua',
        flag_path: 'https://www.residentadvisor.net/images/flags/NU.gif'
    },
    {
        hash: '5d839147c83e283c1d1bb705dc50586f',
        name: 'Nigeria',
        flag_path: 'https://www.residentadvisor.net/images/flags/NG.gif'
    },
    {
        hash: 'd5b9290a0b67727d4ba1ca6059dc31a6',
        name: 'Norway',
        flag_path: 'https://www.residentadvisor.net/images/flags/NO.gif'
    },
    {
        hash: '1c77b9733832da9d1ffac66620f3c006',
        name: 'Oman',
        flag_path: 'https://www.residentadvisor.net/images/flags/OM.gif'
    },
    {
        hash: 'b9698b8546220246fe600a949db326bf',
        name: 'Pakistan',
        flag_path: 'https://www.residentadvisor.net/images/flags/PK.gif'
    },
    {
        hash: '008ced81ddf77a45e35513f4459d7bbf',
        name: 'Palestine',
        flag_path: 'https://www.residentadvisor.net/images/flags/PS.gif'
    },
    {
        hash: '6bec347f256837d3539ad619bd489de7',
        name: 'Panama',
        flag_path: 'https://www.residentadvisor.net/images/flags/PA.gif'
    },
    {
        hash: '73101738da81e5cbb87b64cd400a4405',
        name: 'Paraguay',
        flag_path: 'https://www.residentadvisor.net/images/flags/PY.gif'
    },
    {
        hash: '84c8fa2341f7d052a1ee3a36ff043798',
        name: 'Peru',
        flag_path: 'https://www.residentadvisor.net/images/flags/PE.gif'
    },
    {
        hash: '77dab2f81a6c8c9136efba7ab2c4c0f2',
        name: 'Philippines',
        flag_path: 'https://www.residentadvisor.net/images/flags/PH.gif'
    },
    {
        hash: '94880bda83bda77c5692876700711f15',
        name: 'Poland',
        flag_path: 'https://www.residentadvisor.net/images/flags/PL.gif'
    },
    {
        hash: 'ea71b362e3ea9969db085abfccdeb10d',
        name: 'Portugal',
        flag_path: 'https://www.residentadvisor.net/images/flags/PT.gif'
    },
    {
        hash: 'f76257271129c703d6c0442c8ac00dae',
        name: 'Puerto Rico',
        flag_path: 'https://www.residentadvisor.net/images/flags/PR.gif'
    },
    {
        hash: '7cc7ef17c45527cf90fcf27516794d21',
        name: 'Qatar',
        flag_path: 'https://www.residentadvisor.net/images/flags/QA.gif'
    },
    {
        hash: '0c7d5ae44b2a0be9ebd7d6b9f7d60f20',
        name: 'Romania',
        flag_path: 'https://www.residentadvisor.net/images/flags/RO.gif'
    },
    {
        hash: '5feb168ca8fb495dcc89b1208cdeb919',
        name: 'Russia',
        flag_path: 'https://www.residentadvisor.net/images/flags/RU.gif'
    },
    {
        hash: '9d7ed5d77d647e89c3cc11757d651dc2',
        name: 'Rwanda',
        flag_path: 'https://www.residentadvisor.net/images/flags/RW.gif'
    },
    {
        hash: '5976169ac1852fff6f0b9095100407fc',
        name: 'Saint Kitts and Nevis',
        flag_path: 'https://www.residentadvisor.net/images/flags/KN.gif'
    },
    {
        hash: 'd1ef233007d706d71aa6d46642d5f804',
        name: 'Saint Lucia',
        flag_path: 'https://www.residentadvisor.net/images/flags/LC.gif'
    },
    {
        hash: '26a0859a2c10745426a8c452020830db',
        name: 'Saint Martin',
        flag_path: 'https://www.residentadvisor.net/images/flags/RN.gif'
    },
    {
        hash: '0861134397888c470803c77fb6cd2943',
        name: 'Samoa',
        flag_path: 'https://www.residentadvisor.net/images/flags/WS.gif'
    },
    {
        hash: 'b835b521c29f399c78124c4b59341691',
        name: 'Saudi Arabia',
        flag_path: 'https://www.residentadvisor.net/images/flags/SA.gif'
    },
    {
        hash: '9986531359550785caffb2032622437f',
        name: 'Senegal',
        flag_path: 'https://www.residentadvisor.net/images/flags/sn.gif'
    },
    {
        hash: '2ff6e535bd2f100979a171ad430e642b',
        name: 'Serbia',
        flag_path: 'https://www.residentadvisor.net/images/flags/RS.gif'
    },
    {
        hash: 'd2e4449b45608e33e472d939a73868f7',
        name: 'Seychelles',
        flag_path: 'https://www.residentadvisor.net/images/flags/SC.gif'
    },
    {
        hash: '458e4cbc78201c1aec5fc53a31c59378',
        name: 'Singapore',
        flag_path: 'https://www.residentadvisor.net/images/flags/SG.gif'
    },
    {
        hash: '88a392b5a8d8f73986d83a2deefb0472',
        name: 'Slovakia',
        flag_path: 'https://www.residentadvisor.net/images/flags/SK.gif'
    },
    {
        hash: '00247297c394dd443dc97067830c35f4',
        name: 'Slovenia',
        flag_path: 'https://www.residentadvisor.net/images/flags/SI.gif'
    },
    {
        hash: '921855f753932de762b780405a50bdf7',
        name: 'Somalia',
        flag_path: 'https://www.residentadvisor.net/images/flags/SO.gif'
    },
    {
        hash: 'c89bc418c38da77213c6c6e03cac2510',
        name: 'South Africa',
        flag_path: 'https://www.residentadvisor.net/images/flags/ZA.gif'
    },
    {
        hash: '4d4803b0bb7dab1b0627e4f8277edc5b',
        name: 'South Korea',
        flag_path: 'https://www.residentadvisor.net/images/flags/KR.gif'
    },
    {
        hash: '0a6355f86240762961997e70c5e7087d',
        name: 'South Sudan',
        flag_path: 'https://www.residentadvisor.net/images/flags/SS.gif'
    },
    {
        hash: '907eba32d950bfab68227fd7ea22999b',
        name: 'Spain',
        flag_path: 'https://www.residentadvisor.net/images/flags/ES.gif'
    },
    {
        hash: 'ef547e2d9750443f6d203233dfa38e39',
        name: 'Sri Lanka',
        flag_path: 'https://www.residentadvisor.net/images/flags/LK.gif'
    },
    {
        hash: '74458a3d3e5f3074226b1f9fa23c9163',
        name: 'Sudan',
        flag_path: 'https://www.residentadvisor.net/images/flags/SD.gif'
    },
    {
        hash: 'c8f4261f9f46e6465709e17ebea7a92b',
        name: 'Sweden',
        flag_path: 'https://www.residentadvisor.net/images/flags/SE.gif'
    },
    {
        hash: '3ad08396dc5afa78f34f548eea3c1d64',
        name: 'Switzerland',
        flag_path: 'https://www.residentadvisor.net/images/flags/CH.gif'
    },
    {
        hash: '551fe18ef47d4e6e9d943b9a68ada21d',
        name: 'Taiwan',
        flag_path: 'https://www.residentadvisor.net/images/flags/TW.gif'
    },
    {
        hash: 'b5a9722262bbb7a7449ebc0f4394c901',
        name: 'Tajikistan',
        flag_path: 'https://www.residentadvisor.net/images/flags/TJ.gif'
    },
    {
        hash: 'c03b1123e45fa00da3142e0424ee5eec',
        name: 'Tanzania',
        flag_path: 'https://www.residentadvisor.net/images/flags/TZ.gif'
    },
    {
        hash: '103357e3e40a9c0e4e9d36110f7bbc7a',
        name: 'Thailand',
        flag_path: 'https://www.residentadvisor.net/images/flags/TH.gif'
    },
    {
        hash: '63965a52775c39cd64c3ef0248d585b1',
        name: 'Trinidad and Tobago',
        flag_path: 'https://www.residentadvisor.net/images/flags/TT.gif'
    },
    {
        hash: 'd6dacba23ab4a1d5b72f223bfb010a05',
        name: 'Tunisia',
        flag_path: 'https://www.residentadvisor.net/images/flags/TN.gif'
    },
    {
        hash: '221cdfb73049678e244380b45872cbb2',
        name: 'Turkey',
        flag_path: 'https://www.residentadvisor.net/images/flags/TK.gif'
    },
    {
        hash: '53b3c88ea00c4f0e137b4e6fe7bd23f1',
        name: 'Uganda',
        flag_path: 'https://www.residentadvisor.net/images/flags/UG.gif'
    },
    {
        hash: 'f01fc92b23faa973f3492a23d5a705c5',
        name: 'Ukraine',
        flag_path: 'https://www.residentadvisor.net/images/flags/UA.gif'
    },
    {
        hash: '8f6f28f0d2061af28bcf49d1725b2cbd',
        name: 'United Arab Emirates',
        flag_path: 'https://www.residentadvisor.net/images/flags/AE.gif'
    },
    {
        hash: '89f9c9f489be2a83cf57e53b9197d288',
        name: 'United Kingdom',
        flag_path: 'https://www.residentadvisor.net/images/flags/UK.gif'
    },
    {
        hash: '1f122dd19db580fd03635dd699fb49de',
        name: 'United States of America',
        flag_path: 'https://www.residentadvisor.net/images/flags/US.gif'
    },
    {
        hash: '75497a22409db78dcc52c291e078bc10',
        name: 'Uruguay',
        flag_path: 'https://www.residentadvisor.net/images/flags/UY.gif'
    },
    {
        hash: 'a0a3e881895e46eb96218f0988d405b9',
        name: 'Uzbekistan',
        flag_path: 'https://www.residentadvisor.net/images/flags/UZ.gif'
    },
    {
        hash: 'e95294b730f61c8175550ec244bfcb50',
        name: 'Venezuela',
        flag_path: 'https://www.residentadvisor.net/images/flags/VE.gif'
    },
    {
        hash: 'fdef6daa799e80dbce1b561577b21181',
        name: 'Vietnam',
        flag_path: 'https://www.residentadvisor.net/images/flags/VN.gif'
    },
    {
        hash: '9d5116a2451bc98c2b46b93acbc1b4f0',
        name: 'Zimbabwe',
        flag_path: 'https://www.residentadvisor.net/images/flags/ZW.gif'
    }
]

module.exports = country