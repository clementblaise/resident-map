
let expect = {}

expect.artist = {
    artists: [
        {
            name: 'Kobosil',
            ra_path: '/dj/kobosil',
            hash: '272c41fc8ef48f61ee559bf6a86155a8'
        },
        {
            name: 'Norman Nodge',
            ra_path: '/dj/normannodge',
            hash: '3225e03519080c7c2a8260312f451771'
        },
        {
            name: 'RVR',
            ra_path: '/dj/rvr',
            hash: '637bbd96d3056fc56ff895d883496242'
        }
    ]
}

expect.flyer = {flyer_path: '/images/events/flyer/2019/11/fr-1122-1327152-front.jpg'}

expect.info = {
    date_start: new Date('2019-11-22T22:00:00.000Z'),
    date_end: new Date('2019-11-24T06:00:00.000Z'),
    venue_id: '132934',
    venue_name: 'La Seine Musicale / Seguin',
    address: "La Seine Musicale / Seguin 1 Cours de l'Île Seguin, 92100 Boulogne-Billancourt, France  France"
}

expect.participant = {participant : 33}

module.exports = expect