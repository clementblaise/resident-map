
let expect = {}

expect.artist = {
    artists: [
        {
            name: 'Bicep',
            ra_path: '/dj/bicep',
            hash: '74a29ff94d75bed348e86f54c5478bdf'
        },
        {
            name: 'Bjarki',
            ra_path: '/dj/bjarki',
            hash: '54d95ba72f16b386aa91a08102e01dbd'
        },
        {
            name: 'Charlotte de Witte',
            ra_path: '/dj/charlottedewitte-be',
            hash: '7939288af69d3e861ee6476f7cebaa73'
        },
        {
            name: 'Denis Sulta',
            ra_path: '/dj/denissulta',
            hash: '4a5b6e24f77cf750178b77f7b7de4920'
        },
        {
            name: 'Derrick May',
            ra_path: '/dj/derrickmay',
            hash: 'dc25dc64b57300d6e4043a5b4773541f'
        },
        {
            name: 'Elena Colombi',
            ra_path: '/dj/elenacolombi',
            hash: '51dc51d4ffa2f25ba0e33763964da921'
        },
        {
            name: 'Interstellar Funk',
            ra_path: '/dj/interstellarfunk',
            hash: 'cf74ccafc392830bb0280a176c123d2e'
        },
        {
            name: 'Emma DJ',
            ra_path: '/dj/emmadj',
            hash: 'c863c0f94102b4476f851a4dd6ab8e2a'
        },
        {
            name: 'Honey Dijon',
            ra_path: '/dj/misshoneydijon',
            hash: 'a04e944d9d8d0d707f30360d9847498b'
        },
        {
            name: 'I Hate Models',
            ra_path: '/dj/ihatemodels',
            hash: '3f0702cfbc346b9db282c5a7b7ef96df'
        },
        {
            name: 'Jardin',
            ra_path: '/dj/jardin',
            hash: '3e22fc28f510cef90163578ad8006630'
        },
        {
            name: 'Lorenzo Senni',
            ra_path: '/dj/lorenzosenni',
            hash: '0d2132fbe4c435dbb84841c45f705798'
        },
        {
            name: 'Mad Miran',
            ra_path: '/dj/madmiran',
            hash: 'cd4fe98354350ca0b385b8778432a541'
        },
        {
            name: 'Modeselektor',
            ra_path: '/dj/modeselektor',
            hash: '0c1ab31a0dc00a3a18b41a4840991f06'
        },
        {
            name: 'Mount Kimbie',
            ra_path: '/dj/mountkimbie',
            hash: '9510ad2f513c36d0c13ed0c2ffa0495f'
        },
        {
            name: 'Mor Elian',
            ra_path: '/dj/morelian',
            hash: '2cbc2141ac1844573cc096c1ba983292'
        },
        {
            name: 'Oktober Lieber',
            ra_path: '/dj/oktoberlieber',
            hash: '6c8c5e264af1701e88230bbc5d0572ed'
        },
        {
            name: 'Olivia',
            ra_path: '/dj/olivia',
            hash: 'ba546f8d6d55634ce9106423ee4c5275'
        },
        {
            name: 'Rødhåd',
            ra_path: '/dj/rodhad',
            hash: '9d0fb6791fa43d9d358d166533a53800'
        },
        {
            name: 'The Black Madonna',
            ra_path: '/dj/theblackmadonna',
            hash: 'b73d5f2aee4766ea3de729fe36f55cdb'
        },
        {
            name: 'Bruce',
            ra_path: '/dj/bruce',
            hash: '4be90ed9c41356ec34247e49aec714a9'
        },
        {
            name: 'Clara!',
            ra_path: '/dj/clara-es',
            hash: '115879c9f9cea850f1bed913922c06d4'
        },
        {
            name: 'Daphni',
            ra_path: '/dj/daphni',
            hash: 'ea7f98e846bac568e57d826257d9f452'
        },
        {
            name: 'DEBONAIR',
            ra_path: '/dj/debonair-uk',
            hash: '11aafabba954086e10f0df34603b476e'
        },
        {
            name: 'Helena Hauff',
            ra_path: '/dj/helenahauff',
            hash: '46f4c47a98008a4735da515ddfd7378b'
        },
        {
            name: 'DJ Stingray',
            ra_path: '/dj/djstingray',
            hash: '712618b7b324267f65e134da23f77e2d'
        },
        {
            name: 'Hunee',
            ra_path: '/dj/hunee',
            hash: '56aa48c7ac27c0bc16776ab7ccad48d8'
        },
        {
            name: 'Jon Hopkins',
            ra_path: '/dj/jonhopkins',
            hash: 'bb3d097fd2e57bd792848ca90f58c751'
        },
        {
            name: 'Josey Rebelle',
            ra_path: '/dj/joseyrebelle',
            hash: '2812dc4230155d1ffaa50b1c38bfcac1'
        },
        {
            name: 'Len Faki',
            ra_path: '/dj/lenfaki',
            hash: 'e88ae19557f5ad796e4cfa193c275f33'
        },
        {
            name: 'Motor City Drum Ensemble',
            ra_path: '/dj/mcde',
            hash: 'bc0861843d0a0dd112ac674bf8d50609'
        },
        {
            name: 'Nicola Cruz',
            ra_path: '/dj/nicolacruz',
            hash: '0f74a06304a26ec27a4cced87f2549ee'
        },
        {
            name: 'Overmono',
            ra_path: '/dj/overmono',
            hash: '94918e550e25ede891e9f43b110945f2'
        },
        {
            name: 'Octavian',
            ra_path: '/dj/octavian',
            hash: 'e5e335f90039876806cf03b7ee0ff265'
        },
        {
            name: 'Robert Hood',
            ra_path: '/dj/roberthood',
            hash: '98d73afff1b2051dd5c50acea93f87fe'
        },
        {
            name: 'Toma Kami',
            ra_path: '/dj/tomakami',
            hash: 'b11d3fdd39bf9fc4cbb091634e11d552'
        },
        {
            name: 'Yaeji',
            ra_path: '/dj/yaeji',
            hash: 'ef273d72ac45e34288b66b6090b84788'
        }
    ]
}


expect.flyer = {
    flyer_path: '/images/events/flyer/2019/7/fr-0705-1236764-1404395-front.jpg'
}

expect.info ={
    date_start: new Date('2019-07-05T18:00:00.000Z'),
    date_end: new Date('2019-07-07T05:00:00.000Z'),
    venue_id: '48290',
    venue_name: 'Parc Floral De Paris',
    address: 'Parc Floral De Paris Parc Floral de Paris, Route de la Pyramide, Paris 12e  France'
}

expect.participant = {participant : 279}

module.exports = expect