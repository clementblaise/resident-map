const expect = require('chai').expect
const event = require('../src/service/scrapper/events')
const cheerio = require('cheerio')
const fs = require('fs')
const path = require('path');

const paris = require('./assets/area/paris')

describe("Events for the area paris", () => {

    let rawPage = cheerio.load(fs.readFileSync(path.join(__dirname, './assets/area/paris/index.html')))

    it('should list events', () => {

        let details = event.scrapeEvents(rawPage, {hash: "MD5HASH"})
        expect(details).to.deep.equal(paris.events)
    })

})