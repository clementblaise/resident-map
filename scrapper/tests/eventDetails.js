const expect = require('chai').expect
const eventDetails = require('../src/service/scrapper/eventsDetails')
const cheerio = require('cheerio')
const fs = require('fs')
var path = require('path');

const event1327152 = require('./assets/events/event1327152')
const event1236764 = require('./assets/events/event1236764')




describe("Event details on sample event 1327152", () => {

    let rawPage = cheerio.load(fs.readFileSync(path.join(__dirname, './assets/events/event1327152/index.html')))

    it('should return object with flyer path properties', () => {
        let details = eventDetails.scrapeFlyer(rawPage, {})
        expect(details).to.deep.equal(event1327152.flyer)
    })


    it('should return object with artists properties', () => {
        let details = eventDetails.scrapeArtist(rawPage, {})
        expect(details).to.deep.equal(event1327152.artist)

    })

    it('should return object with infos properties', () => {
        let details = eventDetails.scrapeInfo(rawPage, {})
        expect(details).to.deep.equal(event1327152.info)
    })

    it('should return object with participants', () => {
        let details = eventDetails.scrapeParticipant(rawPage, {})
        expect(details).to.deep.equal(event1327152.participant)
    })


})

describe("Event details on sample event 1236764", () => {

    let rawPage = cheerio.load(fs.readFileSync(path.join(__dirname,'./assets/events/event1236764/index.html')))

    it('should return object with flyer path properties', () => {
        let details = eventDetails.scrapeFlyer(rawPage, {})
        expect(details).to.deep.equal(event1236764.flyer)
    })


    it('should return object with artists properties', () => {
        let details = eventDetails.scrapeArtist(rawPage, {})
        expect(details).to.deep.equal(event1236764.artist)

    })

    it('should return object with infos properties', () => {
        let details = eventDetails.scrapeInfo(rawPage, {})
        expect(details).to.deep.equal(event1236764.info)
    })

    it('should return object with participants', () => {
        let details = eventDetails.scrapeParticipant(rawPage, {})
        expect(details).to.deep.equal(event1236764.participant)
    })



})